var _css;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import { css } from 'glamor';

var styles = css((_css = {
    lineHeight: 1,
    fontFamily: "'Roboto', sans-serif",
    boxSizing: 'border-box',
    margin: 0,
    padding: 0,
    border: 0,
    fontSize: '100%',
    verticalAlign: 'baseline'

}, _defineProperty(_css, ' html, body, div, span, applet, object, iframe,\n    h1, h2, h3, h4, h5, h6, p, blockquote, pre,\n    a, abbr, acronym, address, big, cite, code,\n    del, dfn, em, img, ins, kbd, q, s, samp,\n    small, strike, strong, sub, sup, tt, var,\n    b, u, i, center,\n    dl, dt, dd, ol, ul, li,\n    fieldset, form, label, legend,\n    table, caption, tbody, tfoot, thead, tr, th, td,\n    article, aside, canvas, details, embed,\n    figure, figcaption, footer, header, hgroup,\n    menu, nav, output, ruby, section, summary,\n    time, mark, audio, video ', {
    margin: 0,
    padding: 0,
    border: 0,
    fontSize: '100%',
    font: 'inherit',
    verticalAlign: 'baseline'
}), _defineProperty(_css, ' article, aside, details, figcaption, figure,\n    footer, header, hgroup, menu, nav, section ', {
    display: 'block'
}), _defineProperty(_css, ' ol, ul', {
    listStyle: 'none'
}), _defineProperty(_css, ' blockquote, q', {
    quotes: 'none'
}), _defineProperty(_css, ' blockquote:before, blockquote:after, q:before, q:after', {
    content: '""'
}), _defineProperty(_css, ' div', {
    boxSizing: 'border-box'
}), _defineProperty(_css, ' table', {
    borderCollapse: 'collapse',
    borderSpacing: 0
}), _defineProperty(_css, ' button', {
    fontFamily: 'inherit',
    borderRadius: '0px'
}), _defineProperty(_css, ' button:focus', {
    outline: 'none'
}), _css));

export default styles;