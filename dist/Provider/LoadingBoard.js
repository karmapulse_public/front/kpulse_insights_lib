import React from 'react';
import { css } from 'glamor';

var loader = css.keyframes({
    '0%': {
        transform: 'translate(-50%, -50%) scale(0.0)',
        opacity: 1
    },
    '100%': {
        transform: 'translate(-50%, -50%) scale(1.0)',
        opacity: 0
    }
});

var styles = css({
    width: '100vw',
    height: '100vh',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#000',
    ' .loader': {
        width: 70,
        height: 70,
        position: 'relative'
    },

    ' .dot1, .dot2': {
        width: 70,
        height: 70,
        position: 'absolute',
        top: '50%',
        left: '50%',
        borderRadius: '100%',
        border: '3px solid #fff',
        transform: 'translate(-50%, -50%) scale(0.0)',
        animation: loader + ' 1s infinite ease-in-out'
    },
    ' .dot2': {
        animationDelay: '.5s'
    }
});

var LoadingBoard = function LoadingBoard() {
    return React.createElement(
        'div',
        Object.assign({ className: 'loading-board' }, styles),
        React.createElement(
            'div',
            { className: 'loader' },
            React.createElement('div', { className: 'dot1' }),
            React.createElement('div', { className: 'dot2' })
        )
    );
};

export default LoadingBoard;