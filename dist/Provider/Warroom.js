import _regeneratorRuntime from 'babel-runtime/regenerator';

var _this = this;

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

import React, { useEffect, useState } from 'react';
import fetch from 'isomorphic-fetch';
import isUndefined from 'lodash/isUndefined';
import LoadingBoard from './LoadingBoard';
import ErrorWarroom from '../Components/ErrorWarroom';
import mapLayoutType from '../layouts/mapLayoutType';

var Warroom = function Warroom(props) {
    var _useState = useState({
        loading: true,
        error: false,
        data: null,
        success: false,
        section: ''
    }),
        _useState2 = _slicedToArray(_useState, 2),
        state = _useState2[0],
        setState = _useState2[1];

    var services = {
        develop: {
            config: 'https://dev-serverless.karmapulse.com/insights/get-board-configuration',
            twitter: 'https://dev-serverless.karmapulse.com/insights/recipes-twitter',
            facebook: 'https://dev-serverless.karmapulse.com/elasticsearch-fb/query_recipe_facebook'
        },
        production: {
            config: 'https://serverless.karmapulse.com/insights/get-board-configuration',
            twitter: 'https://serverless.karmapulse.com/insights/recipes-twitter',
            facebook: 'https://serverless.karmapulse.com/elasticsearch-fb/query_recipe_facebook'
        }
    };

    useEffect(function () {
        var env = props.env,
            karmaBoard = props.karmaBoard;

        var fetchData = function () {
            var _ref = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime.mark(function _callee() {
                var uri, response, json, section;
                return _regeneratorRuntime.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                uri = services[env].config + '?content_type=board&fields.slug=' + karmaBoard + '&include=5';
                                _context.next = 3;
                                return fetch(uri);

                            case 3:
                                response = _context.sent;

                                if (response.ok) {
                                    _context.next = 7;
                                    break;
                                }

                                setState(Object.assign({}, state, {
                                    loading: false, error: true, data: response.statusText
                                }));
                                return _context.abrupt('return');

                            case 7:
                                _context.next = 9;
                                return response.json();

                            case 9:
                                json = _context.sent;

                                if (!isUndefined(json.items[0])) {
                                    _context.next = 13;
                                    break;
                                }

                                setState(Object.assign({}, state, {
                                    loading: false, error: true, data: 'no existe información del board'
                                }));
                                return _context.abrupt('return');

                            case 13:
                                section = json.items[0].fields.layout_type[0].fields.section;


                                setState({
                                    data: json.items[0], success: true, loading: false, section: section
                                });

                            case 15:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, _this);
            }));

            return function fetchData() {
                return _ref.apply(this, arguments);
            };
        }();

        fetchData();
    }, []);
    if (state.loading === true) return React.createElement(LoadingBoard, null);
    if (state.error === true) return React.createElement(ErrorWarroom, { status: state });
    return mapLayoutType(state.data.fields, '' + (props.sectionBoard || state.section), services[props.env]);
};

export default Warroom;