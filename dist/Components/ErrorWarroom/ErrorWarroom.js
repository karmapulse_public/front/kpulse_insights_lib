import React from 'react';
import PropTypes from 'prop-types';

var propTypes = {
    classes: PropTypes.object
};

var defaultProps = {
    classes: {}
};

var ModuleError = function ModuleError() {
    return React.createElement(
        'section',
        null,
        React.createElement('img', { src: process.env.PUBLIC_URL + '/img/statusError@2x.png', alt: 'Hubo un error' }),
        React.createElement(
            'div',
            null,
            React.createElement(
                'h1',
                null,
                '\xA1UPS!'
            ),
            React.createElement(
                'p',
                null,
                'El War Room indicado no existe. Verifica que la url sea la correcta o dale clic al siguiente bot\xF3n:'
            )
        )
    );
};

ModuleError.defaultProps = defaultProps;
ModuleError.propTypes = propTypes;

export default ModuleError;