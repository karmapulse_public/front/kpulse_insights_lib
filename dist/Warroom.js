import React from 'react';
import { Context } from './Helpers/ContextHook/context';
import Warroom from './Provider';

var MainRender = function MainRender(props) {
    var _React$useContext = React.useContext(Context),
        state = _React$useContext.state,
        dispatch = _React$useContext.dispatch;

    // React.useEffect(() => {
    //     const { env, karmaBoard } = props;
    //     const initialSection = () => {
    //         dispatch({ type: "CHANGESECTION", payload: karmaBoard })
    //     };

    //     initialSection();
    // }, []);


    return React.createElement(Warroom, Object.assign({}, Object.assign({}, state), { env: props.env, karmaBoard: props.karmaBoard }));
};

export default MainRender;