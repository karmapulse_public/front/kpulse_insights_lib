var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import React, { Component } from 'react';
import moment from 'moment';
import { withConnect } from '../../../helpers/context';

export default (function (_ref) {
    var disabled = _ref.disabled;
    return function (Componente) {
        var HOCSelectTime = function (_Component) {
            _inherits(HOCSelectTime, _Component);

            function HOCSelectTime(props) {
                _classCallCheck(this, HOCSelectTime);

                var _this = _possibleConstructorReturn(this, (HOCSelectTime.__proto__ || Object.getPrototypeOf(HOCSelectTime)).call(this, props));

                _this.interval = null;
                _this.modifyDateRange = _this.modifyDateRange.bind(_this);
                return _this;
            }

            _createClass(HOCSelectTime, [{
                key: 'componentDidMount',
                value: function componentDidMount() {
                    var _this2 = this;

                    clearInterval(this.interval);
                    if (disabled !== undefined) {
                        this.props.changeDisabled(disabled);
                    } else {
                        if (this.props.temporality === 'realtime') {
                            this.interval = setInterval(function () {
                                return _this2.modifyDateRange();
                            }, 60000);
                        }
                    }
                }
            }, {
                key: 'componentWillReceiveProps',
                value: function componentWillReceiveProps(nextProps) {
                    var _this3 = this;

                    var temporality = nextProps.temporality,
                        dateRange = nextProps.dateRange;

                    if (nextProps.changeDisabled !== this.props.changeDisabled) {
                        nextProps.changeDisabled(disabled);
                    }
                    if (temporality && dateRange && nextProps.temporality !== this.props.temporality) {
                        if (temporality === 'search') {
                            clearInterval(this.interval);
                            if (nextProps.dateRange !== this.props.dateRange) {
                                this.props.changeDateRange({
                                    startDate: nextProps.dateRange.startDate,
                                    endDate: nextProps.dateRange.endDate
                                });
                            }
                        } else {
                            this.props.changeDateRange({
                                startDate: moment().subtract(8, 'd').set({
                                    minute: 0,
                                    second: 0,
                                    millisecond: 0
                                }),
                                endDate: moment()
                            });
                            this.interval = setInterval(function () {
                                return _this3.modifyDateRange();
                            }, 60000);
                        }
                    }
                }
            }, {
                key: 'modifyDateRange',
                value: function modifyDateRange() {
                    var _props$dateRange = this.props.dateRange,
                        startDate = _props$dateRange.startDate,
                        endDate = _props$dateRange.endDate;

                    this.props.changeDateRange({
                        startDate: startDate.add(1, 'm').set({
                            minute: 0,
                            second: 0,
                            millisecond: 0
                        }),
                        endDate: endDate.add(1, 'm')
                    });
                }
            }, {
                key: 'componentWillUnmount',
                value: function componentWillUnmount() {
                    clearInterval(this.interval);
                }
            }, {
                key: 'render',
                value: function render() {
                    return React.createElement(Componente, Object.assign({
                        dateRange: this.props.dateRange
                    }, this.props));
                }
            }]);

            return HOCSelectTime;
        }(Component);

        var mapStateToProps = function mapStateToProps(state) {
            return {
                dateRange: state.dateRange === undefined ? {
                    startDate: moment().subtract(8, 'd').set({
                        minute: 0,
                        second: 0,
                        millisecond: 0
                    }),
                    endDate: moment()
                } : state.dateRange,
                temporality: state.temporality === undefined ? 'realtime' : state.temporality,
                disabledCalendar: state.disabled === undefined ? false : state.disabled
            };
        };

        var mapDispatchToProps = function mapDispatchToProps(state) {
            return {
                changeDateRange: state.changeDateRange || function () {
                    return {};
                },
                changeDisabled: state.changeDisabled || function () {
                    return {};
                },
                goToUrl: state.goUrl

            };
        };

        return withConnect(mapStateToProps, mapDispatchToProps)(HOCSelectTime);
    };
});