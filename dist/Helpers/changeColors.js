var LightenDarkenColor = function LightenDarkenColor(col, amt) {
    var color = col;
    var usePound = false;
    if (col[0] === '#') {
        color = col.slice(1);
        usePound = true;
    }
    var num = parseInt(color, 16);
    var r = (num >> 16) + amt;

    if (r > 255) r = 255;else if (r < 0) r = 0;

    var b = (num >> 8 & 0x00FF) + amt;

    if (b > 255) b = 255;else if (b < 0) b = 0;

    var g = (num & 0x0000FF) + amt;

    if (g > 255) g = 255;else if (g < 0) g = 0;

    return (usePound ? '#' : '') + (g | b << 8 | r << 16).toString(16);
};

export default LightenDarkenColor;

// export const convertHex = (hex, opacity) => {
//     const hexT = hex.replace('#', '');
//     const r = parseInt(hexT.substring(0, 2), 16);
//     const g = parseInt(hexT.substring(2, 4), 16);
//     const b = parseInt(hexT.substring(4, 6), 16);

//     return `rgba(${r},${g},${b},${opacity})`;
// };