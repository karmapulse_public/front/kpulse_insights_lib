export var numberWithCommas = function numberWithCommas(num) {
    var number = num || 0;
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
};