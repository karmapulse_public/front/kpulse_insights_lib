import initialState from './initialState';

var RESET = 'RESET';
var CHANGEDATE = 'CHANGEDATE';
var SELECTINITIATIVE = 'SELECTINITIATIVE';
var CHANGETEMPORALITY = 'CHANGETEMPORALITY';
var SETDISABLEDCALENDAR = 'SETDISABLEDCALENDAR';
var CHANGESECTION = 'CHANGESECTION';
var CHANGECALENDARVISIBILITY = 'CHANGECALENDARVISIBILITY';

var reducer = function reducer(state, action) {
    switch (action.type) {
        case RESET:
            return initialState;
        case CHANGEDATE:
            return Object.assign({}, state, { dateRange: action.payload
            });
        case CHANGESECTION:
            return Object.assign({}, state, { sectionBoard: action.payload
            });
        case SELECTINITIATIVE:
            return Object.assign({}, state, { initiativeSelected: action.payload
            });
        case SETDISABLEDCALENDAR:
            return Object.assign({}, state, { disabledCalendar: action.payload
            });
        case CHANGETEMPORALITY:
            return Object.assign({}, state, { temporality: action.payload
            });
        case CHANGECALENDARVISIBILITY:
            return Object.assign({}, state, { calendarVisible: action.payload
            });
        default:
            return initialState;
    }
};

export default reducer;