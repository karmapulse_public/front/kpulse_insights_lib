var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

import React from 'react';
import initialState from './initialState';
import reducer from './reducer';

var Context = React.createContext();
var Consumer = Context.Consumer;

var Provider = function Provider(props) {
    var _React$useReducer = React.useReducer(reducer, initialState),
        _React$useReducer2 = _slicedToArray(_React$useReducer, 2),
        state = _React$useReducer2[0],
        dispatch = _React$useReducer2[1];

    var value = { state: state, dispatch: dispatch };

    /* eslint-disable */
    return React.createElement(
        Context.Provider,
        { value: value },
        props.children
    );
    /* eslint-enable */
};

export { Context, Consumer, Provider };