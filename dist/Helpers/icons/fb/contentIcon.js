/**
 * ContentIcon SVG
 */

import React from 'react';

var ContentIcon = function ContentIcon(props) {
    return (
        /* eslint-disable */
        React.createElement(
            "svg",
            { xmlns: "http://www.w3.org/2000/svg", width: "20", height: "15", viewBox: "0 0 20 15" },
            React.createElement(
                "g",
                { fill: props.color, fillRule: "evenodd" },
                React.createElement("path", { d: "M0 15h20v-2H0zM12 6H8v6h4zM18 0h-4v12h4zM6 4H2v8h4z" })
            )
        )

        /* eslint-enable */

    );
};

export default ContentIcon;