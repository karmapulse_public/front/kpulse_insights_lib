/**
 * InteractionsIcon SVG
 */

import React from 'react';

var InteractionsIcon = function InteractionsIcon(props) {
    return (
        /* eslint-disable */
        React.createElement(
            "svg",
            {
                width: "14px",
                height: "21px",
                viewBox: "0 0 14 21",
                version: "1.1",
                xmlnsXlink: "http://www.w3.org/1999/xlink"
            },
            React.createElement(
                "g",
                {
                    transform: "translate(-22.000000, -257.000000) translate(22.000000, 257.000000)",
                    fill: props.color,
                    stroke: "none",
                    strokeWidth: 1,
                    fillRule: "evenodd"
                },
                React.createElement("path", { d: "M6.566 19H12v-3.219l-3.243-.81a1.001 1.001 0 0 1-.757-.97V9.72L7 9.39V15a1 1 0 0 1-1 1H4.766l1.8 3zM13 21H6c-.35 0-.677-.185-.858-.485l-3-5A1.002 1.002 0 0 1 3 14h2V8a.998.998 0 0 1 1.316-.949l3 1a1 1 0 0 1 .684.95v4.218l3.242.811A1 1 0 0 1 14 15v5a1 1 0 0 1-1 1z" }),
                React.createElement("path", { d: "M12 7.259c0 .943-.255 1.866-.73 2.677l1.889.723a7.417 7.417 0 0 0 .841-3.4C14 3.256 10.86 0 7 0S0 3.256 0 7.259A7.44 7.44 0 0 0 1.014 11l1.937-.74A5.299 5.299 0 0 1 2 7.259c0-2.86 2.243-5.185 5-5.185s5 2.326 5 5.185" })
            )
        )
        /* eslint-enable */

    );
};

export default InteractionsIcon;