/**
 * ChartsIcon SVG
 */

import React from 'react';

var ChartsIcon = function ChartsIcon(active) {
    return (
        /* eslint-disable */
        React.createElement(
            "svg",
            { width: "20px", height: "17px", viewBox: "0 0 20 17", version: "1.1", xmlns: "http://www.w3.org/2000/svg", xmlnsXlink: "http://www.w3.org/1999/xlink" },
            React.createElement(
                "g",
                { id: "Page-1", stroke: "none", strokeWidth: "1", fill: "none", fillRule: "evenodd" },
                React.createElement(
                    "g",
                    { id: "Versus-768", transform: "translate(-14.000000, -101.000000)", fill: "#FFFFFF", opacity: active ? 1 : 0.5 },
                    React.createElement(
                        "g",
                        { id: "BarraLateral", transform: "translate(-1.000000, 0.000000)" },
                        React.createElement(
                            "g",
                            { id: "Fill-765-+-Fill-766", transform: "translate(14.000000, 101.000000)" },
                            React.createElement("path", { d: "M18,15.3 L16,15.3 L16,11.05 L18,11.05 L18,15.3 Z M14,15.3 L12,15.3 L12,12.75 L14,12.75 L14,15.3 Z M9.999,15.3 L7.999,15.3 L7.999,11.05 L9.999,11.05 L9.999,15.3 Z M5.999,15.3 L3.999,15.3 L3.999,12.75 L5.999,12.75 L5.999,15.3 Z M16.999,8.00275 L13.082,4.6733 L9.082,8.9233 L5.999,6.30275 L4.413,7.65 L0.999,7.65 L0.999,15.3 C0.999,16.23755 1.896,17 2.999,17 L18.999,17 C20.102,17 20.999,16.23755 20.999,15.3 L20.999,5.95 L19.413,5.95 L16.999,8.00275 Z", id: "Fill-765" }),
                            React.createElement("path", { d: "M18.999,0.000425 L2.999,0.000425 C1.896,0.000425 0.999,0.763725 0.999,1.700425 L0.999,5.950425 L3.585,5.950425 L5.999,3.898525 L8.916,6.377975 L12.916,2.127975 L16.999,5.598525 L18.585,4.250425 L20.999,4.250425 L20.999,1.700425 C20.999,0.763725 20.102,0.000425 18.999,0.000425", id: "Fill-766" })
                        )
                    )
                )
            )
        )
        /* eslint-enable */

    );
};

export default ChartsIcon;