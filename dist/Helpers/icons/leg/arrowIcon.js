import React from 'react';

var arrowIcon = function arrowIcon(color) {
    return (
        /* eslint-disable */
        React.createElement(
            "svg",
            { xmlns: "http://www.w3.org/2000/svg", width: "22", height: "17", viewBox: "0 0 22 17" },
            React.createElement("path", { fill: color, fillRule: "evenodd", d: "M10.848 14.447l-4.064-4.141H22V6.694H6.784l4.064-4.14L8.342 0 0 8.5 8.342 17z" })
        )
        /* eslint-enable */

    );
};

export default arrowIcon;