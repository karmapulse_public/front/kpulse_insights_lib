import React from 'react';

var searchIcon = function searchIcon(color) {
    return (
        /* eslint-disable */
        React.createElement(
            "svg",
            { xmlns: "http://www.w3.org/2000/svg", width: "15", height: "15", viewBox: "0 0 15 15" },
            React.createElement("path", { fill: "#4C5C6D", fillRule: "evenodd", d: "M10.192 8.967h-.647l-.225-.223a5.274 5.274 0 0 0 1.278-3.445 5.299 5.299 0 1 0-5.3 5.299A5.275 5.275 0 0 0 8.744 9.32l.225.224v.645l4.075 4.069 1.215-1.216-4.066-4.076zm-4.893 0a3.668 3.668 0 1 1 0-7.337 3.669 3.669 0 0 1 0 7.337z", opacity: ".5" })
        )
        /* eslint-enable */

    );
};

export default searchIcon;