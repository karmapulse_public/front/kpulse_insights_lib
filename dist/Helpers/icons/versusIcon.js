/**
 * VersusIcon SVG
 */

import React from 'react';

var VersusIcon = function VersusIcon(active) {
    return (
        /* eslint-disable */
        React.createElement(
            "svg",
            { width: "20px", height: "15px", viewBox: "0 0 20 15", version: "1.1", xmlns: "http://www.w3.org/2000/svg", xmlnsXlink: "http://www.w3.org/1999/xlink" },
            React.createElement(
                "g",
                { id: "Page-1", stroke: "none", strokeWidth: "1", fill: "none", fillRule: "evenodd", opacity: active ? 1 : 0.5 },
                React.createElement(
                    "g",
                    { id: "Versus-768", transform: "translate(-14.000000, -62.000000)", fill: "#FFFFFF" },
                    React.createElement(
                        "g",
                        { id: "BarraLateral", transform: "translate(-1.000000, 0.000000)" },
                        React.createElement("path", { d: "M26,74.6923077 C26,72.7803846 27.343,71.2307692 29,71.2307692 L30,71.2307692 C31.657,71.2307692 33,72.7803846 33,74.6923077 L26,74.6923077 Z M17,74.6923077 C17,72.7803846 18.343,71.2307692 20,71.2307692 L21,71.2307692 C22.657,71.2307692 24,72.7803846 24,74.6923077 L17,74.6923077 Z M20.5,65.4615385 C21.601,65.4615385 22.5,66.5011538 22.5,67.7692308 C22.5,69.0361538 21.601,70.0769231 20.5,70.0769231 C19.399,70.0769231 18.5,69.0361538 18.5,67.7692308 C18.5,66.5011538 19.399,65.4615385 20.5,65.4615385 L20.5,65.4615385 Z M29.5,65.4615385 C30.601,65.4615385 31.5,66.5011538 31.5,67.7692308 C31.5,69.0361538 30.601,70.0769231 29.5,70.0769231 C28.399,70.0769231 27.5,69.0361538 27.5,67.7692308 C27.5,66.5011538 28.399,65.4615385 29.5,65.4615385 L29.5,65.4615385 Z M33,62 L17,62 C15.896,62 15,63.0326923 15,64.3076923 L15,74.6923077 C15,75.9661538 15.896,77 17,77 L33,77 C34.105,77 35,75.9661538 35,74.6923077 L35,64.3076923 C35,63.0326923 34.105,62 33,62 L33,62 Z", id: "Fill-574" })
                    )
                )
            )
        )
        /* eslint-enable */

    );
};

export default VersusIcon;