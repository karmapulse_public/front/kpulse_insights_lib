/**
 * LogoutIcon SVG
 */

import React from 'react';

var LogoutIcon = function LogoutIcon(active) {
    return (
        /* eslint-disable */
        React.createElement(
            "svg",
            { width: "17", height: "14", viewBox: "0 0 17 14", xmlns: "http://www.w3.org/2000/svg" },
            React.createElement(
                "g",
                { fill: "none", fillRule: "evenodd", opacity: ".9" },
                React.createElement("path", { fill: "#FFF", d: "M11.263 8.139L11.25 6.47l-7.423-.059L6.511 3.73 5.32 2.54.642 7.221l4.755 4.755 1.17-1.17L3.84 8.08z" }),
                React.createElement("path", { stroke: "#FFF", strokeWidth: "2", d: "M9.37 4.713V1.331h6.086v11.44h-6.23V9.68" })
            )
        )
        /* eslint-enable */

    );
};

export default LogoutIcon;