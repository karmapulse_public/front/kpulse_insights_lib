/**
 * AudienceIcon SVG
 */

import React from 'react';

var AudienceCtoIcon = function AudienceCtoIcon(props) {
    return (
        /* eslint-disable */
        React.createElement(
            "svg",
            { xmlns: "http://www.w3.org/2000/svg", width: "22", height: "19", viewBox: "0 0 22 19" },
            React.createElement(
                "g",
                { fill: props.color, fillRule: "evenodd" },
                React.createElement("path", { d: "M17.6 6.6a2.2 2.2 0 1 1 .002-4.402A2.2 2.2 0 0 1 17.6 6.6m0-6.6c-2.209 0-4.02 1.635-4.335 3.755a5.866 5.866 0 0 1 3.601 5.411c0 .74-.175 1.564-.45 2.42A33.2 33.2 0 0 0 17.6 13.2S22 7.7 22 4.4A4.4 4.4 0 0 0 17.6 0M2.2 4.4a2.2 2.2 0 1 1 4.402.002A2.2 2.2 0 0 1 2.2 4.4m6.535-.645C8.421 1.635 6.61 0 4.4 0A4.4 4.4 0 0 0 0 4.4c0 3.3 4.4 8.8 4.4 8.8s.513-.643 1.185-1.614c-.276-.856-.451-1.68-.451-2.42a5.866 5.866 0 0 1 3.601-5.41" }),
                React.createElement("path", { d: "M11 12.1a2.2 2.2 0 1 1 .002-4.402A2.2 2.2 0 0 1 11 12.1m0-6.6a4.4 4.4 0 0 0-4.4 4.4c0 3.3 4.4 8.8 4.4 8.8s4.4-5.5 4.4-8.8A4.4 4.4 0 0 0 11 5.5" })
            )
        )
        /* eslint-enable */

    );
};

export default AudienceCtoIcon;