/**
 * ScheduleIcon SVG
 */

import React from 'react';

var defaultProps = {
    size: '18px'
};

var ScheduleIcon = function ScheduleIcon(props) {
    return (
        /* eslint-disable */
        React.createElement(
            'svg',
            { xmlns: 'http://www.w3.org/2000/svg', width: props.size, height: props.size, viewBox: '0 0 18 18' },
            React.createElement('path', { fill: props.color, fillRule: 'evenodd', d: 'M15.545 9.818h-3.272V6.545h3.272v3.273zm-4.909 0H7.364V6.545h3.272v3.273zm0 4.91H7.364v-3.273h3.272v3.272zm-4.909-4.91H2.455V6.545h3.272v3.273zm0 4.91H2.455v-3.273h3.272v3.272zM16.364 1.635h-1.637V0h-1.636v4.09h-1.636V1.637h-4.91V0H4.91v4.09H3.273V1.637H1.636C.734 1.636 0 2.371 0 3.273v12.272c0 .903.734 1.637 1.636 1.637h14.728c.902 0 1.636-.734 1.636-1.637V3.273c0-.902-.734-1.637-1.636-1.637z' })
        )
        /* eslint-enable */

    );
};

ScheduleIcon.defaultProps = defaultProps;

export default ScheduleIcon;