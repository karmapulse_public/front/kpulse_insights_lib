import React from 'react';

var RightArrowIcon = function RightArrowIcon() {
    return (
        /* eslint-disable */
        React.createElement(
            "svg",
            { xmlns: "http://www.w3.org/2000/svg", width: "12", height: "12", viewBox: "0 0 12 12" },
            React.createElement(
                "g",
                { fill: "none", fillRule: "evenodd" },
                React.createElement("path", { d: "M-3-3h18v18H-3z" }),
                React.createElement("path", { fill: "#FFF", fillRule: "nonzero", d: "M6 0L4.942 1.058 9.127 5.25H0v1.5h9.127l-4.185 4.193L6 12l6-6z" })
            )
        )
        /* eslint-enable */

    );
};

export default RightArrowIcon;