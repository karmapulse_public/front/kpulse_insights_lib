import React from 'react';

var ChartSubheaderIcon = function ChartSubheaderIcon() {
    return (
        /* eslint-disable */
        React.createElement(
            "svg",
            { xmlns: "http://www.w3.org/2000/svg", width: "16", height: "16", viewBox: "0 0 16 16" },
            React.createElement(
                "g",
                { fill: "none", fillRule: "evenodd" },
                React.createElement("path", { d: "M-2-2h20v20H-2z" }),
                React.createElement("path", { fill: "#FFF", fillRule: "nonzero", d: "M13.833.5H2.167C1.25.5.5 1.25.5 2.167v11.666c0 .917.75 1.667 1.667 1.667h11.666c.917 0 1.667-.75 1.667-1.667V2.167C15.5 1.25 14.75.5 13.833.5zM5.5 12.167H3.833V6.333H5.5v5.834zm3.333 0H7.167V3.833h1.666v8.334zm3.334 0H10.5V8.833h1.667v3.334z" })
            )
        )
        /* eslint-enable */

    );
};

export default ChartSubheaderIcon;