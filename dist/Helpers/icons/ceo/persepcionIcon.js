/**
 * DialogIcon SVG
 */

import React from 'react';

var PersepcionIcon = function PersepcionIcon(props) {
    return (
        /* eslint-disable */
        React.createElement(
            "svg",
            { xmlns: "http://www.w3.org/2000/svg", width: "10", height: "25", viewBox: "0 0 10 25" },
            React.createElement(
                "g",
                { fill: props.color, fillRule: "evenodd" },
                React.createElement("path", { d: "M8.75 18.75h-7.5v-5h7.5v5zM7.5 5h-5v2.009c-.791.893-2.5 3.1-2.5 5.491v11.25C0 24.44.56 25 1.25 25h7.5c.69 0 1.25-.56 1.25-1.25V12.5c0-2.391-1.709-4.598-2.5-5.491V5zM7.5 3.75v-2.5C7.5.56 6.94 0 6.25 0h-2.5C3.06 0 2.5.56 2.5 1.25v2.5h5z" })
            )
        )

        /* eslint-enable */

    );
};

export default PersepcionIcon;