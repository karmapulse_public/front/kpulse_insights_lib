import React from 'react';

var ArrowIcon = function ArrowIcon() {
    return (
        /* eslint-disable */
        React.createElement(
            "svg",
            { width: "9px", height: "21px", viewBox: "0 0 9 21", version: "1.1", xmlns: "http://www.w3.org/2000/svg", xmlnsXlink: "http://www.w3.org/1999/xlink" },
            React.createElement(
                "g",
                { id: "Monitoreo-legislativo", stroke: "none", strokeWidth: "1", fill: "none", fillRule: "evenodd", strokeLinecap: "round", strokeLinejoin: "round" },
                React.createElement(
                    "g",
                    { id: "HERDEZ-Monitoreo-legislativo", transform: "translate(-227.000000, -449.000000)", stroke: "#FFFFFF", strokeWidth: "2" },
                    React.createElement(
                        "g",
                        { id: "Group-21", transform: "translate(30.000000, 450.000000)" },
                        React.createElement("polyline", { id: "Path-7-Copy-5", points: "198 0 205 8.70833333 198 19" })
                    )
                )
            )
        )
        /* eslint-enable */

    );
};

export default ArrowIcon;