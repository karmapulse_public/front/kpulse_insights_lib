/**
 * DialogIcon SVG
 */

import React from 'react';

var CompetitionIcon = function CompetitionIcon(props) {
    return (
        /* eslint-disable */
        React.createElement(
            "svg",
            { xmlns: "http://www.w3.org/2000/svg", width: "17", height: "20", viewBox: "0 0 17 20" },
            React.createElement("path", { fill: props.color, fillRule: "evenodd", d: "M13 18H6v-3h7v3zm0-18H9a4 4 0 0 0-4 4v6H3V5H2a2 2 0 0 0-2 2v3c0 3 2 5 4 5v3a2 2 0 0 0 2 2h7a2 2 0 0 0 2-2v-3.555c1.19-.693 2-1.968 2-3.445V4a4 4 0 0 0-4-4z" })
        )
        /* eslint-enable */

    );
};

export default CompetitionIcon;