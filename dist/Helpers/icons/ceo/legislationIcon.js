/**
 * FeedbackIcon SVG
 */

import React from 'react';

var LegislationIcon = function LegislationIcon(props) {
    return (
        /* eslint-disable */
        React.createElement(
            "svg",
            { xmlns: "http://www.w3.org/2000/svg", width: "27", height: "21", viewBox: "0 0 27 21" },
            React.createElement("path", { fill: props.color, fillRule: "evenodd", d: "M20.862 18.41a3.689 3.689 0 0 1-3.472-2.455h6.943a3.686 3.686 0 0 1-3.47 2.454zm0-10.848l1.979 5.938h-3.957l1.978-5.938zM4.157 13.5l1.978-5.938L8.114 13.5H4.157zm1.978 4.91a3.689 3.689 0 0 1-3.472-2.455h6.943a3.686 3.686 0 0 1-3.47 2.454zM25.43 13.5l-2.863-8.59h3.206V2.454H14.726V0h-2.454v2.455H1.226v2.454h3.206L1.568 13.5h-1.57v1.227a6.143 6.143 0 0 0 6.137 6.137 6.143 6.143 0 0 0 6.137-6.137V13.5h-1.57L7.839 4.91h4.433v1.226h2.454V4.91h4.433L16.296 13.5h-1.57v1.227a6.143 6.143 0 0 0 6.136 6.137A6.143 6.143 0 0 0 27 14.727V13.5h-1.57z" })
        )
        /* eslint-enable */

    );
};

export default LegislationIcon;