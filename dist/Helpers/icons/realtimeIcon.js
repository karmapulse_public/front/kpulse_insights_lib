import React from 'react';

var RealtimeIcon = function RealtimeIcon(_ref) {
    var color = _ref.color;
    return (
        /* eslint-disable */
        React.createElement(
            "svg",
            { xmlns: "http://www.w3.org/2000/svg", width: "14", height: "14", viewBox: "0 0 14 14" },
            React.createElement(
                "g",
                { fill: color, fillRule: "evenodd", opacity: "1" },
                React.createElement("path", { d: "M3.045 3.045A5.56 5.56 0 0 1 7 1.4c3.088 0 5.6 2.512 5.6 5.6H14c0-3.86-3.14-7-7-7-1.9 0-3.654.762-4.944 2.056L.7.7v4.2h4.2L3.045 3.045zM10.955 10.955A5.56 5.56 0 0 1 7 12.6 5.606 5.606 0 0 1 1.4 7H0c0 3.86 3.14 7 7 7 1.9 0 3.654-.762 4.944-2.057L13.3 13.3V9.1H9.1l1.855 1.855z" })
            )
        )

        /* eslint-enable */

    );
};

export default RealtimeIcon;