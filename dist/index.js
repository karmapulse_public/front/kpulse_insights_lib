import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from './Helpers/ContextHook/context';
import Warroom from './Warroom';
import reset from './reset';
import 'typeface-roboto';

var MainRender = function MainRender(props) {
    return React.createElement(
        Provider,
        null,
        React.createElement(
            'div',
            reset,
            React.createElement(Warroom, props)
        )
    );
};

MainRender.propTypes = {
    karmaBoard: PropTypes.string,
    env: PropTypes.oneOf(['develop', 'production'])
};
MainRender.defaultProps = {
    karmaBoard: null,
    env: 'production'
};

export default MainRender;