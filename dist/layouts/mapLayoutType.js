import React from 'react';
import { LayoutSeguridadInicio, LayoutSeguridadTema } from './LayoutSeguridad';

var getModules = function getModules(layout) {
    var newLayout = Object.assign({}, layout);
    delete newLayout.name;
    delete newLayout.section;
    delete newLayout.color;
    delete newLayout.title;
    delete newLayout.date_init;
    delete newLayout.date_end;
    delete newLayout.tab1_title;
    delete newLayout.tab2_title;
    delete newLayout.tab3_title;

    var components = Object.keys(newLayout).map(function (item) {
        if (typeof newLayout[item].fields === 'undefined') {
            return {
                position: 'modules',
                modules: newLayout[item]
            };
        }
        newLayout[item].fields.position = item;
        return newLayout[item];
    });

    return components;
};

export default (function (board, section, services) {
    var layoutList = board.layout_type;
    var layoutBySection = board.layout_type.filter(function (item) {
        return item.fields.section === section;
    })[0];
    var layoutType = layoutBySection.sys.contentType.sys.id;
    var components = getModules(Object.assign({}, layoutBySection.fields));
    var dateInit = board.initial_date;
    var dateEnd = board.final_date;
    var logo = board.logo;
    var autoWalk = board.walkthrough_auto;
    var timeAuto = layoutBySection.fields.time_auto;
    var color = layoutBySection.fields.color;
    var title = layoutBySection.fields.title;
    var ownerProfile = board.owner_profile;
    var subTopics = layoutBySection.fields.subtopic_list;
    var lastUpdate = layoutBySection.sys.updatedAt;
    var tab1Title = layoutBySection.fields.tab1_title;
    var tab2Title = layoutBySection.fields.tab2_title;
    var tab3Title = layoutBySection.fields.tab3_title;

    var profiles = layoutBySection.fields.profiles_list;

    var globalProps = {
        section: section,
        components: components,
        title: title,
        logo: logo,
        layoutList: layoutList,
        services: services,
        periodStart: dateInit,
        periodEnd: dateEnd
    };

    var layouts = {
        layout_security_main: function layout_security_main() {
            return React.createElement(LayoutSeguridadInicio, Object.assign({}, globalProps, {
                title: title,
                ownerProfile: ownerProfile
            }));
        },
        layout_security_topic: function layout_security_topic() {
            return React.createElement(LayoutSeguridadTema, Object.assign({}, globalProps, {
                ownerProfile: ownerProfile,
                subTopics: subTopics,
                color: color
            }));
        }
    };

    return layouts[layoutType]();
});