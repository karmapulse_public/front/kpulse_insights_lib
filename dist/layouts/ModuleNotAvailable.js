import React from 'react';
import { css } from 'glamor';

var styles = css({
    minWidth: 300,
    height: 150,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFFFFF',
    color: '#707070',

    ' h1:last-child': {
        fontSize: 12,
        fontWeight: 700,
        color: '#AE9BD6'
    }
});

var ModuleNotAvailable = function ModuleNotAvailable() {
    return React.createElement(
        'div',
        Object.assign({ className: 'unassigned-module' }, styles),
        React.createElement(
            'h1',
            null,
            'M\xF3dulo no disponible'
        )
    );
};

export default ModuleNotAvailable;