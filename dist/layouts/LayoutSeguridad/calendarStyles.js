

import LightenDarkenColor from '../../Helpers/changeColors';

var darkerVariable = -20;

// const reactDates = {
//     ' .CalendarDay': {
//         padding: "0",
//         boxSizing: "border-box",
//         color: "#565a5c",
//         cursor: "pointer",
//         verticalAlign: "middle",
//         fontSize: "12px",
//         width: "39px",
//         height: "38px",
//         border: "1px solid #eee"
//     },
//     ' .CalendarDay:active': {
//         background: "#f2f2f2"
//     },
//     ' .CalendarDay__button': {
//         position: "relative",
//         height: "100%",
//         width: "100%",
//         textAlign: "center",
//         background: "none",
//         border: "0",
//         margin: "0",
//         padding: "0",
//         color: "inherit",
//         font: "inherit",
//         lineHeight: "normal",
//         overflow: "visible",
//         cursor: "pointer",
//         boxSizing: "border-box"
//     },
//     ' .CalendarDay__button:active': {
//         outline: "0"
//     },
//     ' .CalendarDay--highlighted-calendar': {
//         background: "#ffe8bc",
//         color: "#565a5c",
//         cursor: "default"
//     },
//     ' .CalendarDay--highlighted-calendar:active': {
//         background: "#007a87"
//     },
//     ' .CalendarDay--outside': {
//         border: "0",
//         cursor: "default"
//     },
//     ' .CalendarDay--outside:active': {
//         background: "#fff"
//     },
//     ' .CalendarDay--hovered': {
//         background: "#e4e7e7",
//         border: "1px double #d4d9d9",
//         color: "inherit"
//     },
//     ' .CalendarDay--blocked-minimum-nights': {
//         color: "#cacccd",
//         background: "#fff",
//         border: "1px solid #e4e7e7",
//         cursor: "default"
//     },
//     ' .CalendarDay--blocked-minimum-nights:active': {
//         background: "#fff"
//     },
//     ' .CalendarDay--selected-span': {
//         background: "#78909c",
//         border: "1px double #78909c",
//         color: "#fff"
//     },
//     ' .CalendarDay--selected-span .CalendarDay--hovered': {
//         background: "#455a64",
//         border: "1px double #455a64"
//     },
//     ' .CalendarDay--selected-span:active': {
//         background: "#455a64",
//         border: "1px double #455a64"
//     },
//     ' .CalendarDay--selected-span .CalendarDay--last-in-range': {
//         borderRight: "#455a64"
//     },
//     ' .CalendarDay--hovered-span': {
//         background: "#78909c",
//         border: "1px double #78909c",
//         color: "#FFF"
//     },
//     ' .CalendarDay--after-hovered-start': {
//         background: "#78909c",
//         border: "1px double #78909c",
//         color: "#FFF"
//     },
//     ' .CalendarDay--selected-start': {
//         background: "#455a64",
//         border: "1px double #455a64",
//         color: "#fff"
//     },
//     ' .CalendarDay--selected-end': {
//         background: "#455a64",
//         border: "1px double #455a64",
//         color: "#fff"
//     },
//     ' .CalendarDay--selected': {
//         background: "#455a64",
//         border: "1px double #455a64",
//         color: "#fff"
//     },
//     ' .CalendarDay--selected-start:active': {
//         background: "#455a64"
//     },
//     ' .CalendarDay--selected-end:active': {
//         background: "#455a64"
//     },
//     ' .CalendarDay--selected: active': {
//         background: "#455a64"
//     },
//     ' .CalendarDay--blocked-calendar': {
//         background: "#cacccd",
//         color: "#82888a",
//         cursor: "default"
//     },
//     ' .CalendarDay--blocked-calendar:active': {
//         background: "#cacccd"
//     },
//     ' .CalendarDay--blocked-out-of-range': {
//         color: "#cacccd",
//         background: "#fff",
//         border: "1px solid #e4e7e7",
//         cursor: "default"
//     },
//     ' .CalendarDay--blocked-out-of-range:active': {
//         background: "#fff"
//     },
//     ' .CalendarMonth': {
//         textAlign: "center",
//         padding: "0 13px",
//         verticalAlign: "top",
//         MozUserSelect: "none",
//         WebkitUserSelect: "none",
//         MsUserSelect: "none",
//         userSelect: "none"
//     },
//     ' .CalendarMonth:first-of-type': {
//         position: "absolute",
//         zIndex: "-1",
//         opacity: "0",
//         pointerEvents: "none"
//     },
//     ' .CalendarMonth table': {
//         borderCollapse: "collapse",
//         borderSpacing: "0",
//         captionCaptionSide: "initial"
//     },
//     ' .CalendarMonth--horizontal': {
//         display: "inline-block",
//         minHeight: "100%"
//     },
//     ' .CalendarMonth--vertical': {
//         display: "block"
//     },
//     ' .CalendarMonth__caption': {
//         color: "#3c3f40",
//         marginTop: "7px",
//         fontSize: "14px",
//         textAlign: "center",
//         marginBottom: "2px",
//         captionSide: "initial"
//     },
//     ' .CalendarMonth--horizontal .CalendarMonth__caption,': {
//         padding: "15px 0 35px"
//     },
//     ' .CalendarMonth--vertical.CalendarMonth__caption': {
//         padding: "15px 0 35px"
//     },
//     ' .CalendarMonth--vertical-scrollable .CalendarMonth__caption': {
//         padding: "5px 0"
//     },
//     ' .CalendarMonthGrid': {
//         background: "#fff",
//         zIndex: "0",
//         textAlign: "left"
//     },
//     ' .CalendarMonthGrid--animating': {
//         WebkitTransition: "-webkit-transform 0.2s ease-in-out",
//         MozTransition: "-moz-transform 0.2s ease-in-out",
//         transition: "transform 0.2s ease-in-out",
//         zIndex: "1"
//     },
//     ' .CalendarMonthGrid--horizontal': {
//         position: "absolute",
//         left: "9px",
//         width: "1200px"
//     },
//     ' .CalendarMonthGrid--vertical': {
//         width: "300px",
//         margin: "0 auto"
//     },
//     ' .CalendarMonthGrid--vertical-scrollable': {
//         width: "300px",
//         margin: "0 auto",
//         overflowY: "scroll"
//     },
//     ' .DayPicker': {
//         background: "#fff",
//         position: "relative",
//         textAlign: "left"
//     },
//     ' .DayPicker--horizontal': {
//         background: "#fff",
//         boxShadow: "0 2px 6px rgba(0, 0, 0, 0.05), 0 0 0 1px rgba(0, 0, 0, 0.07)",
//         borderRadius: "3px"
//     },
//     ' .DayPicker--horizontal.DayPicker--portal': {
//         boxShadow: "none",
//         position: "absolute",
//         left: "50%",
//         top: "50%"
//     },
//     ' .DayPicker--vertical.DayPicker--portal': {
//         position: "initial"
//     },
//     ' .DayPicker__week-headers': {
//         position: "relative"
//     },
//     ' .DayPicker--horizontal .DayPicker__week-headers': {
//         marginLeft: "9px"
//     },
//     ' .DayPicker__week-header': {
//         color: "#757575",
//         position: "absolute",
//         width: "300px",
//         top: "54px",
//         zIndex: "2",
//         padding: "0 13px",
//         fontSize: "12px",
//         textAlign: "left"
//     },
//     ' .DayPicker__week-header ul': {
//         listStyle: "none",
//         margin: "1px 0",
//         paddingLeft: "0"
//     },
//     ' .DayPicker__week-header li': {
//         display: "inline-block",
//         width: "39px",
//         textAlign: "center"
//     },
//     ' .DayPicker--vertical .DayPicker__week-header': {
//         marginLeft: "-150px",
//         left: "50%"
//     },
//     ' .DayPicker--vertical-scrollable': {
//         height: "100%"
//     },
//     ' .DayPicker--vertical-scrollable .DayPicker__week-header': {
//         top: "0",
//         display: "table-row",
//         borderBottom: "1px solid #dbdbdb",
//         background: "white",
//         marginLeft: "0",
//         left: "0",
//         width: "100%",
//         textAlign: "center"
//     },
//     ' .DayPicker--vertical-scrollable .transition-container--vertical': {
//         paddingTop: "20px",
//         height: "100%",
//         position: "absolute",
//         top: "0",
//         bottom: "0",
//         right: "0",
//         left: "0",
//         overflowY: "scroll"
//     },
//     ' .transition-container': {
//         position: "relative",
//         overflow: "hidden",
//         borderRadius: "3px"
//     },
//     ' .transition-container--horizontal': {
//         transition: "height 0.2s ease-in-out"
//     },
//     ' .transition-container--vertical': {
//         width: "100%"
//     },
//     ' .DayPickerNavigation__prev': {
//         cursor: "pointer",
//         lineHeight: "0.78",
//         WebkitUserSelect: "none",
//         ChromeSafariMozUserSelect: "none",
//         FirefoxMsUserSelect: "none",
//         IE10UserSelect: "none"
//     },
//     ' .DayPickerNavigation__next': {
//         cursor: "pointer",
//         lineHeight: "0.78",
//         WebkitUserSelect: "none",
//         ChromeSafariMozUserSelect: "none",
//         FirefoxMsUserSelect: "none",
//         IE10UserSelect: "none"
//     },
//     ' .DayPickerNavigation__prev--default': {
//         border: "1px solid #dce0e0",
//         backgroundColor: "#fff",
//         color: "#757575"
//     },
//     ' .DayPickerNavigation__next--default': {
//         border: "1px solid #dce0e0",
//         backgroundColor: "#fff",
//         color: "#757575"
//     },
//     ' .DayPickerNavigation__prev--default:focus, .DayPickerNavigation__prev--default:hover, .DayPickerNavigation__next--default: focus, .DayPickerNavigation__next--default: hover': {
//         border: "1px solid #c4c4c4"
//     },
//     ' .DayPickerNavigation__prev--default:active, .DayPickerNavigation__next--default: active': {
//         background: "#f2f2f2"
//     },
//     ' .DayPickerNavigation--horizontal': {
//         position: "relative"
//     },
//     ' .DayPickerNavigation--horizontal .DayPickerNavigation__prev, .DayPickerNavigation--horizontal.DayPickerNavigation__next': {
//         borderRadius: "3px",
//         padding: "6px 9px",
//         backgroundColor: "#eceff1",
//         top: "18px",
//         zIndex: "2",
//         position: "absolute"
//     },
//     ' .DayPickerNavigation--horizontal .DayPickerNavigation__prev': {
//         left: "22px"
//     },
//     ' .DayPickerNavigation--horizontal .DayPickerNavigation__next': {
//         right: "22px"
//     },
//     ' .DayPickerNavigation--horizontal .DayPickerNavigation__prev--default svg, .DayPickerNavigation--horizontal.DayPickerNavigation__next--default svg': {
//         height: "10px",
//         width: "10px",
//         fill: "#455a64"
//     },
//     ' .DayPickerNavigation--vertical': {
//         background: "#fff",
//         boxShadow: "0 0 5px 2px rgba(0, 0, 0, 0.1)",
//         position: "absolute",
//         bottom: "0",
//         left: "0",
//         height: "52px",
//         width: "100%",
//         zIndex: "2"
//     },
//     ' .DayPickerNavigation--vertical .DayPickerNavigation__prev, .DayPickerNavigation--vertical.DayPickerNavigation__next': {
//         display: "inline-block",
//         position: "relative",
//         height: "100%",
//         width: "50%"
//     },
//     ' .DayPickerNavigation--vertical .DayPickerNavigation__next--default': {
//         borderLeft: "0"
//     },
//     ' .DayPickerNavigation--vertical .DayPickerNavigation__prev--default, .DayPickerNavigation--vertical.DayPickerNavigation__next--default': {
//         textAlign: "center",
//         fontSize: "2.5em",
//         padding: "5px"
//     },
//     ' .DayPickerNavigation--vertical .DayPickerNavigation__prev--default svg, .DayPickerNavigation--vertical.DayPickerNavigation__next--default svg': {
//         height: "42px",
//         width: "42px",
//         fill: "#484848"
//     },
//     ' .DayPickerNavigation--vertical-scrollable': {
//         position: "relative"
//     },
//     ' .DayPickerNavigation--vertical-scrollable .DayPickerNavigation__next': {
//         width: "100%"
//     },
//     ' .DateInput': {
//         fontWeight: "200",
//         fontSize: "11px",
//         lineHeight: "13px",
//         color: "#FFF",
//         margin: "0",
//         padding: "10px 8px",
//         background: "#607d8b",
//         position: "relative",
//         display: "inline-block",
//         width: "100px",
//         height: "40px",
//         verticalAlign: "middle"
//     },
//     ' .DateInput--with-caret::before, .DateInput--with-caret:: after': {
//         content: '""',
//         display: "inline-block",
//         position: "absolute",
//         bottom: "auto",
//         border: "10px solid transparent",
//         borderTop: "0",
//         left: "22px",
//         zIndex: "2"
//     },
//     ' .DateInput--with-caret::before': {
//         top: "40px",
//         borderBottomColor: "rgba(0, 0, 0, 0.1)"
//     },
//     ' .DateInput--with-caret::after': {
//         top: "41px",
//         borderBottomColor: "#fff"
//     },
//     ' .DateInput--disabled': {
//         background: "#cacccd"
//     },
//     ' .DateInput__input': {
//         opacity: "0",
//         position: "absolute",
//         top: "0",
//         left: "0",
//         border: "0",
//         padding: "0",
//         height: "100%",
//         width: "100%"
//     },
//     ' .DateInput__input[readonly]': {
//         MozUserSelect: "none",
//         WebkitUserSelect: "none",
//         MsUserSelect: "none",
//         userSelect: "none"
//     },
//     ' .DateInput__display-text': {
//         padding: "4px 8px",
//         whiteSpace: "nowrap",
//         textAlign: "center",
//         overflow: "hidden"
//     },
//     ' .DateInput__display-text--has-input': {
//         color: "#FFF"
//     },
//     ' .DateInput__display-text--focused': {
//         background: "#455a64",
//         borderRadius: "3px",
//         color: "#FFF"
//     },
//     ' .DateInput__display-text--disabled': {
//         fontStyle: "italic"
//     },
//     ' .screen-reader-only': {
//         border: "0",
//         clip: "rect(0, 0, 0, 0)",
//         height: "1px",
//         margin: "-1px",
//         overflow: "hidden",
//         padding: "0",
//         position: "absolute",
//         width: "1px"
//     },
//     ' .DateRangePicker': {
//         position: "relative",
//         height: "40px",
//         display: "inline-block"
//     },
//     ' .DateRangePicker__picker': {
//         zIndex: "1",
//         backgroundColor: "#fff",
//         position: "absolute",
//         top: "50px"
//     },
//     ' .DateRangePicker__picker--direction-left': {
//         left: "0"
//     },
//     ' .DateRangePicker__picker--direction-right': {
//         right: "0"
//     },
//     ' .DateRangePicker__picker--portal': {
//         backgroundColor: "rgba(0, 0, 0, 0.3)",
//         position: "fixed",
//         top: "0",
//         left: "0",
//         height: "100%",
//         width: "100%"
//     },
//     ' .DateRangePicker__picker--full-screen-portal': {
//         backgroundColor: "#fff"
//     },
//     ' .DateRangePicker__close': {
//         background: "none",
//         border: "0",
//         color: "inherit",
//         font: "inherit",
//         lineHeight: "normal",
//         overflow: "visible",
//         padding: "15px",
//         cursor: "pointer",
//         position: "absolute",
//         top: "0",
//         right: "0",
//         zIndex: "2"
//     },
//     ' .DateRangePicker__close svg': {
//         height: "15px",
//         width: "15px",
//         fill: "#cacccd"
//     },
//     ' .DateRangePicker__close:hover, .DateRangePicker__close:focus': {
//         color: "#b0b3b4",
//         textDecoration: "none"
//     },
//     ' .DateRangePickerInput': {
//         height: "40px",
//         backgroundColor: "#607d8b",
//         display: "inline-block"
//     },
//     ' .DateRangePickerInput--disabled': {
//         background: "#cacccd"
//     },
//     ' .DateRangePickerInput__arrow': {
//         display: "inline-block",
//         verticalAlign: "middle"
//     },
//     ' .DateRangePickerInput__arrow svg': {
//         verticalAlign: "middle",
//         fill: "#FFF",
//         height: "24px",
//         width: "24px"
//     },
//     ' .DateRangePickerInput__clear-dates': {
//         background: "none",
//         border: "0",
//         color: "inherit",
//         font: "inherit",
//         lineHeight: "normal",
//         overflow: "visible",
//         cursor: "pointer",
//         display: "inline-block",
//         verticalAlign: "middle",
//         padding: "10px",
//         margin: "0 10px 0 5px"
//     },
//     ' .DateRangePickerInput__clear-dates svg': {
//         fill: "#82888a",
//         height: "12px",
//         width: "15px",
//         verticalAlign: "middle"
//     },
//     ' .DateRangePickerInput__clear-dates--hide': {
//         visibility: "hidden"
//     },
//     ' .DateRangePickerInput__clear-dates:focus, .DateRangePickerInput__clear-dates--hover': {
//         background: "#dbdbdb",
//         borderRadius: "50%"
//     },
//     ' .DateRangePickerInput__calendar-icon': {
//         background: "none",
//         border: "0",
//         color: "inherit",
//         font: "inherit",
//         lineHeight: "normal",
//         overflow: "visible",
//         cursor: "pointer",
//         display: "inline-block",
//         verticalAlign: "middle",
//         padding: "10px",
//         margin: "0 5px 0 10px"
//     },
//     ' .DateRangePickerInput__calendar-icon svg': {
//         fill: "#82888a",
//         height: "15px",
//         width: "14px",
//         verticalAlign: "middle"
//     },
//     ' .SingleDatePicker': {
//         position: "relative",
//         display: "inline-block"
//     },
//     ' .SingleDatePicker__picker': {
//         zIndex: "1",
//         backgroundColor: "#fff",
//         position: "absolute",
//         top: "72px"
//     },
//     ' .SingleDatePicker__picker--direction-left': {
//         left: "0"
//     },
//     ' .SingleDatePicker__picker--direction-right': {
//         right: "0"
//     },
//     ' .SingleDatePicker__picker--portal': {
//         backgroundColor: "rgba(0, 0, 0, 0.3)",
//         position: "fixed",
//         top: "0",
//         left: "0",
//         height: "100%",
//         width: "100%"
//     },
//     ' .SingleDatePicker__picker--full-screen-portal': {
//         backgroundColor: "#fff"
//     },
//     ' .SingleDatePicker__close': {
//         background: "none",
//         border: "0",
//         color: "inherit",
//         font: "inherit",
//         lineHeight: "normal",
//         overflow: "visible",
//         padding: "15px",
//         cursor: "pointer",
//         position: "absolute",
//         top: "0",
//         right: "0",
//         zIndex: "2"
//     },
//     ' .SingleDatePicker__close svg': {
//         height: "15px",
//         width: "15px",
//         fill: "#cacccd"
//     },
//     ' .SingleDatePicker__close:hover, .SingleDatePicker__close:focus': {
//         color: "#b0b3b4",
//         textDecoration: "none"
//     },
//     ' .SingleDatePickerInput': {
//         backgroundColor: "#fff",
//         border: "1px solid #dbdbdb"
//     },
//     ' .SingleDatePickerInput__clear-date': {
//         background: "none",
//         border: "0",
//         color: "inherit",
//         font: "inherit",
//         lineHeight: "normal",
//         overflow: "visible",
//         cursor: "pointer",
//         display: "inline-block",
//         verticalAlign: "middle",
//         padding: "10px",
//         margin: "0 10px 0 5px"
//     },
//     ' .SingleDatePickerInput__clear-date svg': {
//         fill: "#82888a",
//         height: "12px",
//         width: "15px",
//         verticalAlign: "middle"
//     },
//     ' .SingleDatePickerInput__clear-date--hide': {
//         visibility: "hidden"
//     },
//     ' .SingleDatePickerInput__clear-date:focus, .SingleDatePickerInput__clear-date--hover': {
//         background: "#dbdbdb",
//         borderRadius: "50%"
//     },
// };

var stylesCommon = function stylesCommon(titleColor, mainColors, subtitleColors) {
    return {
        // ...reactDates,
        ' .CalendarMonth': {
            textAlign: "center",
            padding: "0 13px",
            verticalAlign: "top",
            MozUserSelect: "none",
            WebkitUserSelect: "none",
            MsUserSelect: "none",
            userSelect: "none"
        },
        ' .CalendarMonth:first-of-type': {
            position: "absolute",
            zIndex: "-1",
            opacity: "0",
            pointerEvents: "none"
        },
        ' .CalendarMonth--horizontal': {
            display: 'inline-block',
            minHeight: '100%'
        },
        ' .DayPicker__focus-region': {
            outline: 'none'
        },
        ' .DateRangePicker, .DateRangePickerInput, .DateInput': {
            height: 'auto'
        },
        ' .CalendarDay--blocked, .CalendarDay--blocked.CalendarDay--hovered': {
            backgroundColor: LightenDarkenColor(mainColors[0], darkerVariable),
            opacity: 0.3,
            ' button': {
                cursor: 'default'
            }
        },
        ' .CalendarDay--valid': {
            backgroundColor: LightenDarkenColor(mainColors[0], darkerVariable)
        },
        ' .DateInput': {
            padding: '0px',
            width: '90px',
            backgroundColor: mainColors[0]
        },
        ' .DateInput__display-text--focused': {
            backgroundColor: 'transparent'
        },
        ' .DateInput--with-caret::after, .DateInput--with-caret::before, .DayPicker--horizontal, .CalendarMonthGrid--horizontal': {
            backgroundColor: LightenDarkenColor(mainColors[0], darkerVariable),
            borderBottom: LightenDarkenColor(mainColors[0], darkerVariable)
        },
        ' .DateRangePickerInput': {
            backgroundColor: mainColors[0]
        },
        ' .DateRangePicker__picker': {
            backgroundColor: LightenDarkenColor(mainColors[0], darkerVariable),
            boxShadow: '0px 0px 4px 0px rgba(0, 0, 0, 0.5)'
        },
        ' .DayPickerNavigation--horizontal': {
            ' button': {
                top: '15px',
                width: '20px',
                height: '20px',
                margin: '0px 15px',
                backgroundColor: 'rgba(0, 0, 0, 0)',
                border: 'none',
                ' svg': { display: 'none' }
            },
            ' button:after': {
                content: '""',
                top: '50%',
                left: '50%',
                position: 'absolute',
                padding: '3.5px',
                border: 'solid ' + subtitleColors[1]
            },
            ' button:nth-child(2):after': {
                borderWidth: '1.5px 1.5px 0px 0px',
                transform: 'translate(-50%, -50%) rotate(45deg)'
            },
            ' button:first-child:after': {
                borderWidth: '1.5px 0px 0px 1.5px',
                transform: 'translate(-50%, -50%) rotate(-45deg)'
            }
        },
        ' .CalendarDay': {
            border: 'none',
            borderRadius: '3px',
            ' button': {
                fontSize: '1.07em',
                fontWeight: 400
            },
            '&--selected, &--hovered, &--selected-span, &--hovered-span, &--after-hovered-start': {
                backgroundColor: subtitleColors[0]
            },
            '&--selected-start, &--selected-end, &:active, &--hovered': {
                backgroundColor: titleColor
            }
        },
        ' .CalendarMonth__caption': {
            marginTop: "7px",
            fontSize: "14px",
            textAlign: "center",
            marginBottom: "2px",
            captionSide: "initial",
            padding: '11px 0 35px',
            textTransform: 'uppercase',
            color: subtitleColors[1],
            fontWeight: 400
        },
        ' .CalendarMonth table': {
            borderCollapse: 'separate',
            borderSpacing: '3px'
        },
        ' .DayPicker': {
            '&__week-header': {
                padding: '0px',
                color: subtitleColors[1],
                fontWeight: '400',
                opacity: 0.6
            },
            '&__week-header li': {
                width: '42px !important'
            },
            '&__week-header:first-child': {
                left: '13px !important'
            },
            '&__week-header:nth-child(2)': {
                left: '336px !important'
            },
            '&--horizontal': {
                width: '660px !important'
            }
        },
        ' .transition-container--horizontal': {
            width: '660px !important'
        }
    };
};

export default stylesCommon;