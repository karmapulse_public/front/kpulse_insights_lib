var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

import React, { useContext, useState } from 'react';
import { PropTypes } from 'prop-types';
// import { Link } from 'react-router-dom';
import moment from 'moment';

import mapModuleType, { renderComponentByPosition } from 'kinsights_board_modules';
import ChartSubheaderIcon from '../../../Helpers/icons/seg/chartSubheaderIcon';
import RightArrowIcon from '../../../Helpers/icons/seg/rightArrowIcon';
import TemporalityChange from '../components/TemporalityChange';

import stylesCommon from '../layoutSegStyles';
import stylesMain from './styles';
import FiltersMenu from '../components/FiltersMenu';
import { Context } from '../../../Helpers/ContextHook/context';

var propTypes = {
    components: PropTypes.array,
    title: PropTypes.string,
    logo: PropTypes.object,
    layoutList: PropTypes.array,
    ownerProfile: PropTypes.object,
    periodStart: PropTypes.string,
    color: PropTypes.string,
    periodEnd: PropTypes.string,
    subTopics: PropTypes.array,
    subTopicsFilters: PropTypes.string
};

var defaultProps = {
    section: '',
    components: [],
    title: '',
    logo: {},
    layoutList: [],
    ownerProfile: {},
    periodStart: '',
    periodEnd: '',
    color: '#5db6e6',
    subTopics: [],
    subTopicsFilters: ''
};

var LayoutSeguridadTema = function LayoutSeguridadTema(props) {
    var _useContext = useContext(Context),
        state = _useContext.state,
        dispatch = _useContext.dispatch;

    var _useState = useState({
        mapComponents: mapModuleType(props.components),
        selectedOne: 0
    }),
        _useState2 = _slicedToArray(_useState, 2),
        states = _useState2[0],
        setState = _useState2[1];

    var LABELS = { x: 'Horas', y: 'Número de apariciones' };

    var getFilterTopic = function getFilterTopic() {
        var selectedOne = states.selectedOne;
        var subTopics = props.subTopics;


        if (selectedOne === 0) {
            return {};
        }
        return {
            entity: 'phrases',
            params: subTopics[selectedOne].fields.filter_list.join()
        };
    };
    var selectTopic = function selectTopic(index) {
        setState(Object.assign({}, states, {
            selectedOne: index
        }));
    };
    var goToURL = function goToURL() {
        dispatch({ type: "CHANGESECTION", payload: layoutList[0].fields.section });
    };
    var ownerProfile = props.ownerProfile,
        layoutList = props.layoutList,
        logo = props.logo,
        title = props.title,
        color = props.color,
        subTopics = props.subTopics,
        services = props.services,
        periodStart = props.periodStart,
        periodEnd = props.periodEnd;
    var selectedOne = states.selectedOne;
    var _ownerProfile$fields = ownerProfile.fields,
        title_color = _ownerProfile$fields.title_color,
        subtitle_theme = _ownerProfile$fields.subtitle_theme,
        main_theme = _ownerProfile$fields.main_theme,
        theme_image = _ownerProfile$fields.theme_image;

    var filters = getFilterTopic();
    return React.createElement(
        'section',
        Object.assign({}, stylesCommon(title_color, subtitle_theme, main_theme), stylesMain(main_theme, subtitle_theme, color), { className: 'layout-seg-main' }),
        React.createElement(
            'main',
            null,
            React.createElement(
                'div',
                { className: 'seg__subtitle' },
                React.createElement(
                    'button',
                    { className: 'seg__subtitle__arrow', onClick: goToURL },
                    React.createElement(RightArrowIcon, null)
                ),
                React.createElement('div', { className: 'seg__subtitle__separator' }),
                React.createElement(
                    'h1',
                    null,
                    title
                ),
                React.createElement(
                    'div',
                    { className: 'seg__subtitle__info' },
                    React.createElement(
                        'h2',
                        null,
                        React.createElement(ChartSubheaderIcon, null),
                        React.createElement(
                            'span',
                            null,
                            'Medici\xF3n:'
                        ),
                        React.createElement(TemporalityChange, { buttonsColor: subtitle_theme.split(',')[1], initialStartDate: moment(periodStart), initialEndDate: moment(periodEnd) })
                    )
                )
            ),
            React.createElement(FiltersMenu, { topics: subTopics, selectedOne: selectedOne, selectTopic: selectTopic }),
            React.createElement(
                'div',
                { className: 'seg__modules--main' },
                React.createElement(
                    'div',
                    { className: 'seg__container' },
                    renderComponentByPosition(states.mapComponents, 'module_1', {
                        services: services,
                        color: color,
                        dateRange: state.dateRange,
                        labels: LABELS,
                        filters: filters,
                        withCache: false
                    }),
                    React.createElement(
                        'div',
                        null,
                        renderComponentByPosition(states.mapComponents, 'module_2', {
                            services: services,
                            color: color,
                            size: {
                                width: 676,
                                height: 445
                            },
                            dateRange: state.dateRange,
                            filters: filters,
                            withCache: false
                        }),
                        renderComponentByPosition(states.mapComponents, 'module_4', {
                            services: services,
                            color: color,
                            section: props.section,
                            dateRange: state.dateRange,
                            viewLabels: 'new',
                            filters: filters,
                            withCache: false
                        })
                    ),
                    React.createElement(
                        'div',
                        null,
                        renderComponentByPosition(states.mapComponents, 'module_3', {
                            services: services,
                            color: color,
                            size: {
                                width: 560,
                                height: 340
                            },
                            section: props.section,
                            dateRange: state.dateRange,
                            filters: filters,
                            withCache: false,
                            type: state.temporality
                        }),
                        renderComponentByPosition(states.mapComponents, 'module_5', {
                            services: services,
                            color: color,
                            section: props.section,
                            dateRange: state.dateRange,
                            filters: filters,
                            size: 15,
                            withCache: false
                        })
                    )
                )
            )
        )
    );
};

LayoutSeguridadTema.propTypes = propTypes;
LayoutSeguridadTema.defaultProps = defaultProps;

export default LayoutSeguridadTema;