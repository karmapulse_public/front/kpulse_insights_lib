var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

import React, { useContext, useState } from 'react';
import { PropTypes } from 'prop-types';
import moment from 'moment';

import mapModuleType, { renderComponentByPosition } from 'kinsights_board_modules';
import ChartSubheaderIcon from '../../../Helpers/icons/seg/chartSubheaderIcon';

import stylesCommon from '../layoutSegStyles';
import TemporalityChange from '../components/TemporalityChange';
import stylesMain from './styles';

import { Context } from '../../../Helpers/ContextHook/context';

var propTypes = {
    section: PropTypes.string,
    components: PropTypes.array,
    title: PropTypes.string,
    logo: PropTypes.object,
    layoutList: PropTypes.array,
    ownerProfile: PropTypes.object,
    periodStart: PropTypes.string,
    periodEnd: PropTypes.string,
    topicVisualization: PropTypes.string
};

var defaultProps = {
    section: '',
    components: [],
    title: '',
    logo: {},
    layoutList: [],
    ownerProfile: {},
    periodStart: '',
    periodEnd: '',
    topicVisualization: 'area'
};

var LayoutSeguridadInicio = function LayoutSeguridadInicio(props) {
    var _useContext = useContext(Context),
        state = _useContext.state,
        dispatch = _useContext.dispatch;

    var _useState = useState({
        mapComponents: mapModuleType(props.components),
        dataList: [],
        shouldUpdate: true
    }),
        _useState2 = _slicedToArray(_useState, 2),
        states = _useState2[0],
        setState = _useState2[1];

    var LABELS = { x: 'Horas', y: 'Número de apariciones' };

    var addGeneralData = function addGeneralData(data, fields) {
        var dataList = states.dataList,
            shouldUpdate = states.shouldUpdate;

        var indexTitle = dataList.findIndex(function (elto) {
            return elto.title === fields.title;
        });

        if (indexTitle >= 0) {
            dataList[indexTitle] = Object.assign({}, dataList[indexTitle], data);
        } else {
            dataList.push(Object.assign({}, data, {
                color: fields.color,
                title: fields.title
            }));
        }

        if (state.temporality === 'realtime') {
            if (shouldUpdate) {
                setState(Object.assign({}, states, {
                    shouldUpdate: !(props.layoutList.length - 1 === dataList.length),
                    dataList: dataList
                }));
            }
        } else {
            setState(Object.assign({}, states, {
                dataList: dataList
            }));
        }
    };

    var goToURL = function goToURL(e) {
        dispatch({ type: "CHANGESECTION", payload: '/' + e });
    };

    var ownerProfile = props.ownerProfile,
        layoutList = props.layoutList,
        title = props.title,
        services = props.services,
        periodStart = props.periodStart;
    var _ownerProfile$fields = ownerProfile.fields,
        title_color = _ownerProfile$fields.title_color,
        subtitle_theme = _ownerProfile$fields.subtitle_theme,
        main_theme = _ownerProfile$fields.main_theme;

    return React.createElement(
        'section',
        Object.assign({}, stylesCommon(title_color, subtitle_theme, main_theme), stylesMain(main_theme), { className: 'layout-seg-main' }),
        React.createElement(
            'main',
            null,
            React.createElement(
                'div',
                { className: 'seg__subtitle' },
                React.createElement(
                    'h1',
                    null,
                    title
                ),
                React.createElement(
                    'h2',
                    null,
                    React.createElement(ChartSubheaderIcon, null),
                    React.createElement(
                        'span',
                        null,
                        'Medici\xF3n:'
                    ),
                    React.createElement(TemporalityChange, { buttonsColor: subtitle_theme.split(',')[1], initialStartDate: moment(periodStart) })
                )
            ),
            React.createElement(
                'div',
                { className: 'seg__modules--main' },
                React.createElement(
                    'div',
                    { className: 'seg__container' },
                    renderComponentByPosition(states.mapComponents, 'module_1', {
                        data: layoutList.length - 1 === states.dataList.length ? states.dataList : [],
                        color: main_theme.split(',')[0],
                        labels: LABELS,
                        withCache: false,
                        dateRange: state.dateRange
                    }),
                    React.createElement(
                        'div',
                        { className: 'seg__container__topics' },
                        layoutList.map(function (layout, index) {
                            if (index !== 0) {
                                var fields = layout.fields;


                                return renderComponentByPosition(states.mapComponents, 'module_2', {
                                    services: services,
                                    key: index,
                                    color: fields.color,
                                    topic: {
                                        slug: fields.section,
                                        title: fields.title,
                                        search_id: fields.search_id
                                    },
                                    dateRange: state.dateRange,
                                    extraFunction: function extraFunction(data) {
                                        addGeneralData(data, fields);
                                    },
                                    withCache: false,
                                    goToURL: goToURL
                                });
                            }
                            return null;
                        })
                    )
                )
            )
        )
    );
};

LayoutSeguridadInicio.propTypes = propTypes;
LayoutSeguridadInicio.defaultProps = defaultProps;

export default LayoutSeguridadInicio;