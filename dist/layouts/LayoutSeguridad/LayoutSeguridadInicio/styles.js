import { css } from 'glamor';
import LightenDarkenColor from '../../../Helpers/changeColors';

var stylesMain = function stylesMain(mainTheme) {
    var mainColors = mainTheme.split(',');
    return css({
        ' .topicSight__topic .recharts-layer .recharts-cartesian-axis, .topicSight__topic .recharts-legend-wrapper, .topicSight__topic .recharts-cartesian-grid-horizontal': {
            display: 'none'
        },
        ' .seg__container': {
            '>div:first-of-type, >div .topicSight__topic': {
                backgroundColor: mainColors[0],
                borderRadius: 4,
                boxShadow: '0px 0px 4px 0px rgba(0, 0, 0, 0.1)',
                transition: 'all 0.25s ease'
            },
            '&__topics': {
                display: 'flex',
                flexWrap: 'wrap',
                justifyContent: 'flex-start',
                margin: '0px auto',
                ' >div:last-child': {
                    marginRight: '0px'
                },
                ' >div': { minWidth: '290px' }
            }
        },
        ' .seg__subtitle': {
            justifyContent: 'space-between'
        },
        ' .noData': {
            height: '100% !important',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center'
        },
        ' .topicSight__topic': {
            padding: '15px 10px 10px',
            marginTop: '30px',
            fontWeight: 400,
            color: mainColors[2],
            textAlign: 'center',
            ':hover': {
                ' h1 svg': {
                    marginLeft: '5px'
                },
                ' h1 svg path:last-child': {
                    fill: mainColors[2]
                }
            },
            ' span': {
                color: '#b5b5b5',
                fontSize: '0.75em'
            },
            ' h1': {
                letterSpacing: '1.2px',
                fontSize: '1em'
            },
            ' h2': {
                marginTop: '10px',
                fontSize: '2.125em',
                fontWeight: 700,
                letterSpacing: '0.6px'
            },
            '&__chart': {
                marginTop: 20,
                borderRadius: 5,
                border: 'solid 1px ' + mainColors[3]
            }
        },
        ' .line-chart .tooltip_custom': {
            backgroundColor: LightenDarkenColor(mainColors[0], -20),
            color: mainColors[2]
        }
    });
};

export default stylesMain;