var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

/* eslint-disable react/no-unused-prop-types */
import React, { useContext, useState } from 'react';
import moment from 'moment';
import omit from 'lodash/omit';
import '../../datepicker.css';
import { Context } from '../../../../Helpers/ContextHook/context';
import { DayPickerRangeController, DayPickerRangeControllerShape } from 'react-dates';

var propTypes = Object.assign({}, DayPickerRangeControllerShape);

var defaultProps = {
    // example props for the demo
    autoFocusEndDate: false,
    initialStartDate: null,
    initialEndDate: null,
    startDateOffset: undefined,
    endDateOffset: undefined,
    showInputs: false,
    minDate: null,
    maxDate: null,

    // day presentation and interaction related props
    renderCalendarDay: undefined,
    renderDayContents: null,
    minimumNights: 0,
    isDayBlocked: function isDayBlocked() {
        return false;
    },
    isDayHighlighted: function isDayHighlighted() {
        return false;
    },
    enableOutsideDays: false,

    // calendar presentation and interaction related props
    orientation: 'horizontal',
    verticalHeight: undefined,
    withPortal: false,
    initialVisibleMonth: null,
    numberOfMonths: 2,
    keepOpenOnDateSelect: false,
    renderCalendarInfo: null,
    isRTL: false,
    renderMonthText: null,
    renderMonthElement: null,
    hideKeyboardShortcutsPanel: true,
    calendarInfoPosition: 'bottom',

    // navigation related props
    navPrev: null,
    navNext: null,
    onPrevMonthClick: function onPrevMonthClick() {},
    onNextMonthClick: function onNextMonthClick() {},


    // internationalization
    monthFormat: 'MMMM YYYY'
};

var DatePicker = function DatePicker(props) {
    var _useContext = useContext(Context),
        state = _useContext.state,
        dispatch = _useContext.dispatch;

    var calendarVisible = state.calendarVisible,
        temporality = state.temporality;

    var _useState = useState({
        DPDates: { startDate: null, endDate: null },
        beforeDPDates: { startDate: null, endDate: null }
    }),
        _useState2 = _slicedToArray(_useState, 2),
        states = _useState2[0],
        setState = _useState2[1];

    if (states.DPDates.startDate === null || states.beforeDPDates.startDate === null) {
        setState({
            DPDates: Object.assign({}, state.dateRange),
            beforeDPDates: Object.assign({}, state.dateRange)
        });
    }

    var _useState3 = useState(props.autoFocusEndDate ? 'endDate' : 'startDate'),
        _useState4 = _slicedToArray(_useState3, 2),
        focusedInput = _useState4[0],
        setfocusedInput = _useState4[1];

    var changeDateRange = function changeDateRange(payload) {
        dispatch({ type: 'CHANGEDATE', payload: payload });
    };

    var canSeeCalendar = function canSeeCalendar(payload) {
        dispatch({ type: 'CHANGECALENDARVISIBILITY', payload: payload });
    };

    var onOutsideClick = function onOutsideClick() {
        canSeeCalendar(false);
    };

    var cancelDates = function cancelDates() {
        var _states$beforeDPDates = states.beforeDPDates,
            startDate = _states$beforeDPDates.startDate,
            endDate = _states$beforeDPDates.endDate;

        setState(Object.assign({}, states, {
            DPDates: {
                startDate: startDate.clone(),
                endDate: endDate.clone()
            }
        }));
        onOutsideClick();
    };

    var setDates = function setDates() {
        var _states$DPDates = states.DPDates,
            startDate = _states$DPDates.startDate,
            endDate = _states$DPDates.endDate;

        if (!endDate) return;
        var finalStarDate = startDate ? startDate.clone().set({
            hours: 0,
            minutes: 0,
            seconds: 0
        }) : null;
        var finalEndDate = endDate || startDate.clone();
        finalEndDate.set({
            hours: 23,
            minutes: 59,
            seconds: 59
        });
        changeDateRange({
            startDate: finalStarDate,
            endDate: finalEndDate
        });
        setState(Object.assign({}, states, {
            beforeDPDates: {
                endDate: finalEndDate.clone(),
                startDate: finalStarDate.clone()
            }
        }));
        onOutsideClick();
    };

    var _isOutsideRange = function _isOutsideRange(date, startDate) {
        return date.isBefore(startDate.set({ hours: 0 })) || date.isAfter(moment().set({ hours: 23, minutes: 59, seconds: 59 }));
    };

    var onDatesChange = function onDatesChange(_ref) {
        var startDate = _ref.startDate,
            endDate = _ref.endDate;

        startDate.utcOffset(-6);
        setState(Object.assign({}, states, {
            DPDates: {
                startDate: startDate,
                endDate: endDate
            }
        }));
    };

    var onFocusChange = function onFocusChange(focusedInput) {
        setfocusedInput(
        // Force the focusedInput to always be truthy so that dates are always selectable
        !focusedInput ? 'startDate' : focusedInput);
    };

    var propsF = omit(props, ['autoFocus', 'autoFocusEndDate', 'initialStartDate', 'initialEndDate', 'showInputs', 'isOutsideRange', 'onOutsideClick']);
    return calendarVisible && temporality === 'search' ? React.createElement(
        'div',
        { className: 'c-date-picker' },
        React.createElement(DayPickerRangeController, Object.assign({}, propsF, {
            onDatesChange: onDatesChange,
            onFocusChange: onFocusChange,
            onOutsideClick: cancelDates,
            isOutsideRange: function isOutsideRange(date) {
                return _isOutsideRange(date, props.initialStartDate);
            },
            renderCalendarInfo: function renderCalendarInfo() {
                return React.createElement(
                    'div',
                    { className: 'c-date-picker__footer' },
                    React.createElement(
                        'button',
                        { onClick: cancelDates },
                        React.createElement(
                            'span',
                            null,
                            'Cancelar'
                        )
                    ),
                    React.createElement(
                        'button',
                        { className: !states.DPDates.endDate ? 'disabled' : '', onClick: setDates },
                        React.createElement(
                            'span',
                            null,
                            'Aceptar'
                        )
                    )
                );
            },
            focusedInput: focusedInput,
            startDate: states.DPDates.startDate,
            endDate: states.DPDates.endDate
        }))
    ) : null;
};

DatePicker.propTypes = propTypes;
DatePicker.defaultProps = defaultProps;

export default DatePicker;