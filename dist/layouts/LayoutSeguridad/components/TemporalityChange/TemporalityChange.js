import React, { Fragment, useContext, useEffect } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { Context } from '../../../../Helpers/ContextHook/context';

import DatePicker from '../DatePicker';
import RealtimeIcon from '../../../../Helpers/icons/realtimeIcon';
import CalendarIcon from '../../../../Helpers/icons/calendarIcon';
var propTypes = {
    buttonsColor: PropTypes.string
};

var defaultProps = {
    buttonsColor: '#FFF'
};

var setDateToCero = function setDateToCero(date) {
    return date.set({
        hours: 0,
        minute: 0,
        second: 0,
        millisecond: 0
    });
};

var TemporalityChange = function TemporalityChange(_ref) {
    var buttonsColor = _ref.buttonsColor,
        initialStartDate = _ref.initialStartDate;

    var _useContext = useContext(Context),
        _useContext$state = _useContext.state,
        dateRange = _useContext$state.dateRange,
        temporality = _useContext$state.temporality,
        dispatch = _useContext.dispatch;

    var startDate = dateRange.startDate,
        endDate = dateRange.endDate;
    // Set dates to get correct intervals

    var initStartDate = setDateToCero(initialStartDate);
    var renderDates = function renderDates() {
        return React.createElement(
            Fragment,
            null,
            React.createElement(
                'span',
                null,
                startDate.format('L')
            ),
            React.createElement(
                'svg',
                { xmlns: 'http://www.w3.org/2000/svg', width: '10', height: '5', viewBox: '0 0 10 5' },
                React.createElement('path', { fill: '#D8D8D8', fillRule: 'evenodd', d: 'M7.354.147l-.708.707L7.793 2H0v1h7.793L6.646 4.147l.708.707L9.707 2.5z', opacity: '.68' })
            ),
            React.createElement(
                'span',
                null,
                endDate.format('L')
            )
        );
    };

    var onClickRT = function onClickRT() {
        if (temporality === 'search') {
            dispatch({ type: 'CHANGETEMPORALITY', payload: 'realtime' });
        }
    };

    var onClickS = function onClickS() {
        if (temporality === 'realtime') {
            dispatch({ type: 'CHANGETEMPORALITY', payload: 'search' });
        }
        dispatch({ type: 'CHANGECALENDARVISIBILITY', payload: true });
    };

    useEffect(function () {
        if (temporality === 'realtime') {
            var interval = setInterval(function () {
                dispatch({
                    type: 'CHANGEDATE',
                    payload: {
                        startDate: moment().subtract(1, 'd').set({
                            minute: 0,
                            second: 0,
                            millisecond: 0
                        }),
                        endDate: moment()
                    }
                });
            }, 60000);
            dispatch({
                type: 'CHANGEDATE',
                payload: {
                    startDate: moment().subtract(1, 'd').set({
                        minute: 0,
                        second: 0,
                        millisecond: 0
                    }),
                    endDate: moment()
                }
            });
            return function () {
                return clearInterval(interval);
            };
        }
    }, [temporality]);

    return React.createElement(
        'div',
        { className: 'seg__subtitle__temporality' },
        React.createElement(
            'button',
            { className: 'button__realtime ' + (temporality === 'realtime' ? 'active' : ''), onClick: onClickRT },
            React.createElement(RealtimeIcon, { color: buttonsColor })
        ),
        React.createElement(
            'button',
            { className: 'button__search ' + (temporality === 'search' ? 'active' : ''), onClick: onClickS },
            React.createElement(CalendarIcon, { color: buttonsColor })
        ),
        React.createElement(
            'div',
            { className: 'seg__subtitle__temporality-info' },
            React.createElement(
                'h3',
                null,
                'Periodo:'
            ),
            React.createElement(
                'p',
                null,
                temporality === 'search' ? renderDates() : 'Últimas 24 horas'
            )
        ),
        React.createElement(DatePicker, { initialStartDate: initStartDate })
    );
};

TemporalityChange.propTypes = propTypes;
TemporalityChange.defaultProps = defaultProps;

export default TemporalityChange;