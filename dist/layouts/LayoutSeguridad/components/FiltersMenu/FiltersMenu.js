import React from 'react';
import PropTypes from 'prop-types';

var propTypes = {
    topics: PropTypes.array,
    selectedOne: PropTypes.number,
    selectTopic: PropTypes.func
};

var defaultProps = {
    topics: [],
    selectedOne: 0,
    selectTopic: function selectTopic() {
        return {};
    }
};

var FiltersMenu = function FiltersMenu(_ref) {
    var topics = _ref.topics,
        selectedOne = _ref.selectedOne,
        selectTopic = _ref.selectTopic;

    if (topics[0] !== 'Todos los datos') {
        topics.splice(0, 0, 'Todos los datos');
    }

    return React.createElement(
        'div',
        { className: 'seg__subtitle__topics' },
        React.createElement(
            'p',
            null,
            'Filtrar por:'
        ),
        topics.map(function (topic, index) {
            if (index === 0) {
                return React.createElement(
                    'button',
                    { key: topic, onClick: function onClick() {
                            return selectTopic(index);
                        }, className: '' + (selectedOne === index ? 'active' : 'inactive') },
                    topic
                );
            }
            var topicName = topic.fields.topic;
            return React.createElement(
                'button',
                { key: topicName, onClick: function onClick() {
                        return selectTopic(index);
                    }, className: '' + (selectedOne === index ? 'active' : 'inactive') },
                topicName
            );
        })
    );
};

FiltersMenu.propTypes = propTypes;
FiltersMenu.defaultProps = defaultProps;

export default FiltersMenu;