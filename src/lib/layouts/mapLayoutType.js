import React from 'react';
import {
    LayoutSeguridadInicio,
    LayoutSeguridadTema
} from './LayoutSeguridad';

const getModules = (layout) => {
    const newLayout = Object.assign({}, layout);
    delete newLayout.name;
    delete newLayout.section;
    delete newLayout.color;
    delete newLayout.title;
    delete newLayout.date_init;
    delete newLayout.date_end;
    delete newLayout.tab1_title;
    delete newLayout.tab2_title;
    delete newLayout.tab3_title;

    const components = (Object.keys(newLayout)).map((item) => {
        if (typeof newLayout[item].fields === 'undefined') {
            return {
                position: 'modules',
                modules: newLayout[item]
            };
        }
        newLayout[item].fields.position = item;
        return newLayout[item];
    });

    return components;
};

export default (board, section, services) => {
    const layoutList = board.layout_type;
    const layoutBySection = board.layout_type.filter(item => (
        item.fields.section === section
        ))[0];
    const layoutType = layoutBySection.sys.contentType.sys.id;
    const components = getModules(Object.assign({}, layoutBySection.fields));
    const dateInit = board.initial_date;
    const dateEnd = board.final_date;
    const logo = board.logo;
    const autoWalk = board.walkthrough_auto;
    const timeAuto = layoutBySection.fields.time_auto;
    const color = layoutBySection.fields.color;
    const title = layoutBySection.fields.title;
    const ownerProfile = board.owner_profile;
    const subTopics = layoutBySection.fields.subtopic_list;
    const lastUpdate = layoutBySection.sys.updatedAt;
    const tab1Title = layoutBySection.fields.tab1_title;
    const tab2Title = layoutBySection.fields.tab2_title;
    const tab3Title = layoutBySection.fields.tab3_title;

    const profiles = layoutBySection.fields.profiles_list;
    

    const globalProps = {
        section,
        components,
        title,
        logo,
        layoutList,
        services,
        periodStart: dateInit,
        periodEnd: dateEnd,
    };

    const layouts = {
        layout_security_main: () => (
            <LayoutSeguridadInicio
                {...globalProps}
                title={title}
                ownerProfile={ownerProfile}
            />
        ),
        layout_security_topic: () => (
            <LayoutSeguridadTema
                {...globalProps}
                ownerProfile={ownerProfile}
                subTopics={subTopics}
                color={color}
            />
        )
    };

    return layouts[layoutType]();
};
