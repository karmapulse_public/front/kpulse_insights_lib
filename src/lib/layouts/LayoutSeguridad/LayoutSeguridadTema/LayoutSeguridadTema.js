import React, { useContext, useState } from 'react';
import { PropTypes } from 'prop-types';
// import { Link } from 'react-router-dom';
import moment from 'moment';

import mapModuleType, { renderComponentByPosition } from 'kinsights_board_modules';
import ChartSubheaderIcon from '../../../Helpers/icons/seg/chartSubheaderIcon';
import RightArrowIcon from '../../../Helpers/icons/seg/rightArrowIcon';
import TemporalityChange from '../components/TemporalityChange';

import stylesCommon from '../layoutSegStyles';
import stylesMain from './styles';
import FiltersMenu from '../components/FiltersMenu';
import { Context } from '../../../Helpers/ContextHook/context';


const propTypes = {
    components: PropTypes.array,
    title: PropTypes.string,
    logo: PropTypes.object,
    layoutList: PropTypes.array,
    ownerProfile: PropTypes.object,
    periodStart: PropTypes.string,
    color: PropTypes.string,
    periodEnd: PropTypes.string,
    subTopics: PropTypes.array,
    subTopicsFilters: PropTypes.string
};

const defaultProps = {
    section: '',
    components: [],
    title: '',
    logo: {},
    layoutList: [],
    ownerProfile: {},
    periodStart: '',
    periodEnd: '',
    color: '#5db6e6',
    subTopics: [],
    subTopicsFilters: ''
};

const LayoutSeguridadTema = (props) => {
    const { state, dispatch } = useContext(Context);
    const [states, setState] = useState({
        mapComponents: mapModuleType(props.components),
        selectedOne: 0
    });
    const LABELS = { x: 'Horas', y: 'Número de apariciones' };

    const getFilterTopic = () => {
        const {
            selectedOne
        } = states;

        const {
            subTopics
        } = props;

        if (selectedOne === 0) {
            return {};
        }
        return {
            entity: 'phrases',
            params: subTopics[selectedOne].fields.filter_list.join()
        };
    };
    const selectTopic = (index) => {
        setState({
            ...states,
            selectedOne: index
        });
    };
    const goToURL = () => {
        dispatch({ type: "CHANGESECTION", payload: layoutList[0].fields.section })
    };
    const {
        ownerProfile,
        layoutList,
        logo,
        title,
        color,
        subTopics,
        services,
        periodStart,
        periodEnd
    } = props;
    
    const {
        selectedOne,
    } = states;
    
    const {
        title_color,
        subtitle_theme,
        main_theme,
        theme_image
    } = ownerProfile.fields;
    const filters = getFilterTopic();
    return (
        <section {...stylesCommon(title_color, subtitle_theme, main_theme)} {...stylesMain(main_theme, subtitle_theme, color)} className="layout-seg-main">
            <main>
                <div className="seg__subtitle">
                    <button className="seg__subtitle__arrow" onClick={goToURL}>
                        <RightArrowIcon />
                    </button>
                    <div className="seg__subtitle__separator" />
                    <h1>{title}</h1>
                    <div className="seg__subtitle__info">
                        <h2>
                            <ChartSubheaderIcon />
                            <span>Medición:</span>
                            <TemporalityChange buttonsColor={subtitle_theme.split(',')[1]} initialStartDate={moment(periodStart)} initialEndDate={moment(periodEnd)} />
                        </h2>
                    </div>
                </div>
                <FiltersMenu topics={subTopics} selectedOne={selectedOne} selectTopic={selectTopic} />
                <div className="seg__modules--main">
                    <div className="seg__container">
                        {
                            renderComponentByPosition(
                                states.mapComponents,
                                'module_1',
                                {
                                    services,
                                    color,
                                    dateRange: state.dateRange,
                                    labels: LABELS,
                                    filters,
                                    withCache: false
                                }
                            )
                        }
                        <div>
                            {
                                renderComponentByPosition(
                                    states.mapComponents,
                                    'module_2',
                                    {
                                        services,
                                        color,
                                        size: {
                                            width: 676,
                                            height: 445
                                        },
                                        dateRange: state.dateRange,
                                        filters,
                                        withCache: false
                                    }
                                )
                            }
                            {
                                renderComponentByPosition(
                                    states.mapComponents,
                                    'module_4',
                                    {
                                        services,
                                        color,
                                        section: props.section,
                                        dateRange: state.dateRange,
                                        viewLabels: 'new',
                                        filters,
                                        withCache: false
                                    }
                                )
                            }
                        </div>
                        <div>
                            {
                                renderComponentByPosition(
                                    states.mapComponents,
                                    'module_3',
                                    {
                                        services,
                                        color,
                                        size: {
                                            width: 560,
                                            height: 340
                                        },
                                        section: props.section,
                                        dateRange: state.dateRange,
                                        filters,
                                        withCache: false,
                                        type: state.temporality
                                    }
                                )
                            }
                            {
                                renderComponentByPosition(
                                    states.mapComponents,
                                    'module_5',
                                    {
                                        services,
                                        color,
                                        section: props.section,
                                        dateRange: state.dateRange,
                                        filters,
                                        size: 15,
                                        withCache: false
                                    }
                                )
                            }
                        </div>
                    </div>
                </div>
            </main>
        </section>
    );
};

LayoutSeguridadTema.propTypes = propTypes;
LayoutSeguridadTema.defaultProps = defaultProps;


export default LayoutSeguridadTema;
