import { css } from 'glamor';

const stylesMain = (mainTheme, subtitleTheme, color) => {
    const mainColors = mainTheme.split(',');
    const subtitleColors = subtitleTheme.split(',');
    return css({
        ' .seg__container>div:first-child, .seg__container>div:not(:first-child)>div': {
            backgroundColor: mainColors[0],
            borderRadius: 4,
            boxShadow: '0px 0px 4px 0px rgba(0, 0, 0, 0.1)',
        },
        ' .seg__container>div:not(:first-child)': {
            '>div': { marginTop: 20 },
            '&:nth-child(2n)': { width: '55%' },
            '&:nth-child(2n+1)': { width: 'calc(45% - 20px)', marginLeft: '20px' }
        },
        ' .seg__subtitle': {
            '&__arrow': {
                width: '18px',
                height: '18px',
                padding: 0,
                border: 'none',
                backgroundColor: 'transparent',     
                ' svg': {
                    width: 'inherit',
                    height: 'inherit',
                    transform: 'rotateY(180deg)',
                    cursor: 'pointer',
                }
            },
            '&__separator': {
                width: '2px',
                height: '100%',
                margin: '0px 20px',
                backgroundColor: subtitleColors[1],
                opacity: 0.15,
            },
            '&__topics': {
                padding: '0px 105px',
                margin: '25px 0px 0px',
                color: subtitleColors[1],
                ' p': {
                    marginBottom: '15px',
                    fontSize: '0.875em',
                },
                ' button': {
                    display: 'inline-block',
                    padding: '0.3125em 1.25em',
                    marginRight: '10px',
                    fontSize: '0.625em',
                    color: subtitleColors[1],
                    border: 'none',
                    borderRadius: 2,
                    background: subtitleColors[0],
                    textTransform: 'uppercase',
                    cursor: 'pointer',
                    transition: 'background-color 0.25s ease'
                },
                ' button.active, button:hover': {
                    backgroundColor: color
                },
            },
            '&__info': {
                display: 'inherit',
                height: '100%',
                marginLeft: 'auto',
                alignItems: 'inherit',
            }
        },
        ' .line-chart .tooltip_custom': {
            backgroundColor: color,
            color: '#FFF',
            ' ul li': {
                color: '#FFF !important'
            }
        },
        ' .container__footer': {
            marginTop: 35,
        },
        ' .container__footer__reactions': {
            display: 'flex',
            justifyContent: 'space-between',
            width: 225,
            ' div': {
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                marginRight: '0px !important',
            },
            ' button': {
                color: 'transparent',
                borderColor: 'transparent',
                background: 'transparent',
                marginRight: '11px',
                padding: '0px',
                cursor: 'pointer'
            },

            ' span': {
                color: `${mainColors[2]}`,
                fontSize: '0.750em',
                fontWeight: '600'
            }
        },
        ' .m-treemap-trends': {
            maxHeight: 415,
            position: 'relative',
        },
        ' .column-data__label': {
            fontFamily: 'Roboto',
            fontSize: '0.68em !important',
            opacity: 0.5,
            stroke: '#fff',
            textAnchor: 'middle',
            letterSpacing: '0.5px'
        },
        ' .treemap-trends': {
            '&__chart': {
                width: '100%',
                padding: 15,
            },
            '&__footer': {
                top: 0,
                right: 0,
                display: 'flex',
                color: `${mainColors[1]}`,
                position: 'absolute',
                width: '184px !important',
                padding: '13px 15px 0px 0px',
                justifyContent: 'space-between',
                ' >h5': {
                    fontSize: '0.75em',
                    fontFamily: 'Roboto',
                    '&::before': {
                        top: '50% !important',
                        left: '-25% !important',
                        transform: 'translate(0, -50%)'
                    }
                }
            }
        },
        ' .feed-new-data': {
            padding: '13px 0px',
            backgroundColor: '#151c1f',
            color: '#FFF',
            border: 'none',
            fontSize: '0.75em',
        }
    });
};

export default stylesMain;
