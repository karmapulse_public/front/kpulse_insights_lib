import React, { useContext, useState } from 'react';
import { PropTypes } from 'prop-types';
import moment from 'moment';

import mapModuleType, { renderComponentByPosition } from 'kinsights_board_modules';
import ChartSubheaderIcon from '../../../Helpers/icons/seg/chartSubheaderIcon';

import stylesCommon from '../layoutSegStyles';
import TemporalityChange from '../components/TemporalityChange';
import stylesMain from './styles';

import { Context } from '../../../Helpers/ContextHook/context';

const propTypes = {
    section: PropTypes.string,
    components: PropTypes.array,
    title: PropTypes.string,
    logo: PropTypes.object,
    layoutList: PropTypes.array,
    ownerProfile: PropTypes.object,
    periodStart: PropTypes.string,
    periodEnd: PropTypes.string,
    topicVisualization: PropTypes.string
};

const defaultProps = {
    section: '',
    components: [],
    title: '',
    logo: {},
    layoutList: [],
    ownerProfile: {},
    periodStart: '',
    periodEnd: '',
    topicVisualization: 'area'
};

const LayoutSeguridadInicio = (props) => {
    const { state, dispatch} = useContext(Context);
    const [states, setState] = useState({
        mapComponents: mapModuleType(props.components),
        dataList: [],
        shouldUpdate: true,
    });
    const LABELS = { x: 'Horas', y: 'Número de apariciones' };

    const addGeneralData = (data, fields) => {
        const { dataList, shouldUpdate } = states;
        const indexTitle = dataList.findIndex(elto => elto.title === fields.title);

        if (indexTitle >= 0) {
            dataList[indexTitle] = {
                ...dataList[indexTitle],
                ...data
            };
        } else {
            dataList.push({
                ...data,
                color: fields.color,
                title: fields.title
            });
        }

        if (state.temporality === 'realtime') {
            if (shouldUpdate) {
                setState({
                    ...states,
                    shouldUpdate: !(props.layoutList.length - 1 === dataList.length),
                    dataList
                });
            }
        } else {
            setState({
                ...states,
                dataList
            });
        }
    };

    const goToURL = (e) => {
        dispatch({ type: "CHANGESECTION", payload: `/${e}` })
    };

    const {
        ownerProfile,
        layoutList,
        title,
        services,
        periodStart,
    } = props;
    const {
        title_color,
        subtitle_theme,
        main_theme,
    } = ownerProfile.fields;
    return (
        <section {...stylesCommon(title_color, subtitle_theme, main_theme)} {...stylesMain(main_theme)} className="layout-seg-main">
            <main>
                <div className="seg__subtitle">
                    <h1>{title}</h1>
                    <h2>
                        <ChartSubheaderIcon />
                        <span>Medición:</span>
                        <TemporalityChange buttonsColor={subtitle_theme.split(',')[1]} initialStartDate={moment(periodStart)}/>
                    </h2>
                </div>
                <div className="seg__modules--main">
                    <div className="seg__container">
                        {
                            renderComponentByPosition(
                                states.mapComponents,
                                'module_1',
                                {
                                    data: layoutList.length - 1 === states.dataList.length ? states.dataList : [],
                                    color: main_theme.split(',')[0],
                                    labels: LABELS,
                                    withCache: false,
                                    dateRange: state.dateRange
                                }
                            )
                        }
                        <div className="seg__container__topics">
                            {
                                layoutList.map((layout, index) => {
                                    if (index !== 0) {
                                        const { fields } = layout;

                                        return (
                                            renderComponentByPosition(
                                                states.mapComponents,
                                                'module_2',
                                                {
                                                    services: services,
                                                    key: index,
                                                    color: fields.color,
                                                    topic: {
                                                        slug: fields.section,
                                                        title: fields.title,
                                                        search_id: fields.search_id,
                                                    },
                                                    dateRange: state.dateRange,
                                                    extraFunction: (data) => {
                                                        addGeneralData(data, fields);
                                                    },
                                                    withCache: false,
                                                    goToURL
                                                }
                                            )
                                        );
                                    }
                                    return null;
                                })
                            }
                        </div>
                    </div>
                </div>
            </main>
        </section>
    );
};

LayoutSeguridadInicio.propTypes = propTypes;
LayoutSeguridadInicio.defaultProps = defaultProps;

export default LayoutSeguridadInicio;
