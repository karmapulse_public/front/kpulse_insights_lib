import React, { Fragment, useContext, useEffect } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { Context } from '../../../../Helpers/ContextHook/context';

import DatePicker from '../DatePicker';
import RealtimeIcon from '../../../../Helpers/icons/realtimeIcon';
import CalendarIcon from '../../../../Helpers/icons/calendarIcon';
const propTypes = {
    buttonsColor: PropTypes.string,
};

const defaultProps = {
    buttonsColor: '#FFF',
};

const setDateToCero = (date) => (
    date.set({
        hours: 0,
        minute: 0,
        second: 0,
        millisecond: 0
    })
);

const TemporalityChange =
    ({
        buttonsColor,
        initialStartDate,
    }) => {
        const { state: { dateRange, temporality }, dispatch } = useContext(Context);
        const { startDate, endDate } = dateRange;
        // Set dates to get correct intervals
        const initStartDate = setDateToCero(initialStartDate);
        const renderDates = () => (
            <Fragment>
                <span>{startDate.format('L')}</span>
                <svg xmlns="http://www.w3.org/2000/svg" width="10" height="5" viewBox="0 0 10 5">
                    <path fill="#D8D8D8" fillRule="evenodd" d="M7.354.147l-.708.707L7.793 2H0v1h7.793L6.646 4.147l.708.707L9.707 2.5z" opacity=".68" />
                </svg>
                <span>{endDate.format('L')}</span>
            </Fragment>
        );

        const onClickRT = () => {
            if (temporality === 'search') {
                dispatch({ type: 'CHANGETEMPORALITY', payload: 'realtime' });
            }
        };

        const onClickS = () => {
            if (temporality === 'realtime') { dispatch({ type: 'CHANGETEMPORALITY', payload: 'search' }); }
            dispatch({ type: 'CHANGECALENDARVISIBILITY', payload: true });
        };


        useEffect(
            () => {
                if (temporality === 'realtime') {
                    let interval = setInterval(() => {
                        dispatch({
                            type: 'CHANGEDATE',
                            payload: {
                                startDate: moment().subtract(1, 'd').set({
                                    minute: 0,
                                    second: 0,
                                    millisecond: 0
                                }),
                                endDate: moment()
                            }
                        });
                    }, 60000);
                    dispatch({
                        type: 'CHANGEDATE',
                        payload: {
                            startDate: moment().subtract(1, 'd').set({
                                minute: 0,
                                second: 0,
                                millisecond: 0
                            }),
                            endDate: moment()
                        }
                    });
                    return () => clearInterval(interval);
                }
            },
            [temporality]
        );

        return (
            <div className="seg__subtitle__temporality">
                <button className={`button__realtime ${temporality === 'realtime' ? 'active' : ''}`} onClick={onClickRT}>
                    <RealtimeIcon color={buttonsColor} />
                </button>
                <button className={`button__search ${temporality === 'search' ? 'active' : ''}`} onClick={onClickS}>
                    <CalendarIcon color={buttonsColor} />
                </button>
                <div className="seg__subtitle__temporality-info">
                    <h3>Periodo:</h3>
                    <p>
                        {temporality === 'search' ? renderDates() : 'Últimas 24 horas'}
                    </p>
                </div>
                <DatePicker initialStartDate={initStartDate} />
            </div>
        );
    };

TemporalityChange.propTypes = propTypes;
TemporalityChange.defaultProps = defaultProps;

export default TemporalityChange;
