import React from 'react';
import PropTypes from 'prop-types';

const propTypes = {
    topics: PropTypes.array,
    selectedOne: PropTypes.number,
    selectTopic: PropTypes.func
};

const defaultProps = {
    topics: [],
    selectedOne: 0,
    selectTopic: () => ({})
};

const FiltersMenu = ({ topics, selectedOne, selectTopic }) => {
    if (topics[0] !== 'Todos los datos') {
        topics.splice(0, 0, 'Todos los datos');
    }

    return (
        <div className="seg__subtitle__topics">
            <p>Filtrar por:</p>
            {
                topics.map((topic, index) => {
                    if (index === 0) {
                        return (<button key={topic} onClick={() => selectTopic(index)} className={`${selectedOne === index ? 'active' : 'inactive'}`}>
                            {topic}
                        </button>);
                    }
                    const topicName = topic.fields.topic;
                    return (
                        <button key={topicName} onClick={() => selectTopic(index)} className={`${selectedOne === index ? 'active' : 'inactive'}`}>
                            {topicName}
                        </button>
                    );
                })
            }
        </div>
    );
};

FiltersMenu.propTypes = propTypes;
FiltersMenu.defaultProps = defaultProps;

export default FiltersMenu;
