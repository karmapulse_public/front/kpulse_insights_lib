/* eslint-disable react/no-unused-prop-types */
import React, { useContext, useState } from 'react';
import moment from 'moment';
import omit from 'lodash/omit';
import '../../datepicker.css';
import { Context } from '../../../../Helpers/ContextHook/context';
import { DayPickerRangeController, DayPickerRangeControllerShape } from 'react-dates';

const propTypes = {
    ...DayPickerRangeControllerShape
};

const defaultProps = {
    // example props for the demo
    autoFocusEndDate: false,
    initialStartDate: null,
    initialEndDate: null,
    startDateOffset: undefined,
    endDateOffset: undefined,
    showInputs: false,
    minDate: null,
    maxDate: null,

    // day presentation and interaction related props
    renderCalendarDay: undefined,
    renderDayContents: null,
    minimumNights: 0,
    isDayBlocked: () => false,
    isDayHighlighted: () => false,
    enableOutsideDays: false,

    // calendar presentation and interaction related props
    orientation: 'horizontal',
    verticalHeight: undefined,
    withPortal: false,
    initialVisibleMonth: null,
    numberOfMonths: 2,
    keepOpenOnDateSelect: false,
    renderCalendarInfo: null,
    isRTL: false,
    renderMonthText: null,
    renderMonthElement: null,
    hideKeyboardShortcutsPanel: true,
    calendarInfoPosition: 'bottom',

    // navigation related props
    navPrev: null,
    navNext: null,
    onPrevMonthClick() { },
    onNextMonthClick() { },

    // internationalization
    monthFormat: 'MMMM YYYY',
};

const DatePicker = (props) => {
    const { state, dispatch } = useContext(Context);
    const { calendarVisible, temporality  } = state;
    const [states, setState] = useState({
        DPDates: { startDate: null, endDate: null },
        beforeDPDates: { startDate: null, endDate: null }
    });
    if (states.DPDates.startDate === null || states.beforeDPDates.startDate === null) {
        setState({
            DPDates: { ...state.dateRange },
            beforeDPDates: { ...state.dateRange }
        });
    }
    const [focusedInput, setfocusedInput] = useState(props.autoFocusEndDate ? 'endDate' : 'startDate');


    const changeDateRange = (payload) => {
        dispatch({ type: 'CHANGEDATE', payload });
    };

    const canSeeCalendar = (payload) => {
        dispatch({ type: 'CHANGECALENDARVISIBILITY', payload });
    };

    const onOutsideClick = () => {
        canSeeCalendar(false);
    }

    const cancelDates = () => {
        const { startDate, endDate } = states.beforeDPDates;
        setState({
            ...states,
            DPDates: {
                startDate: startDate.clone(),
                endDate: endDate.clone()
            }
        });
        onOutsideClick();
    }

    const setDates = () => {
        const { startDate, endDate } = states.DPDates;
        if (!endDate) return;
        const finalStarDate = startDate ? startDate.clone().set({
            hours: 0,
            minutes: 0,
            seconds: 0
        }) : null;
        const finalEndDate = endDate || startDate.clone();
        finalEndDate.set({
            hours: 23,
            minutes: 59,
            seconds: 59
        });
        changeDateRange({
            startDate: finalStarDate,
            endDate: finalEndDate,
        });
        setState({
            ...states,
            beforeDPDates: {
                endDate: finalEndDate.clone(),
                startDate: finalStarDate.clone()
            }
        });
        onOutsideClick();
    }

    const isOutsideRange = (date, startDate) => (
        date.isBefore(startDate.set({ hours: 0 })) || date.isAfter(moment().set({ hours: 23, minutes: 59, seconds: 59 }))
    );

    const onDatesChange = ({ startDate, endDate }) => {
        startDate.utcOffset(-6);
        setState({
            ...states,
            DPDates: {
                startDate,
                endDate
            },
        });
    };

    const onFocusChange = (focusedInput) => {
        setfocusedInput(
            // Force the focusedInput to always be truthy so that dates are always selectable
            !focusedInput ? 'startDate' : focusedInput,
        );
    }

    const propsF = omit(props, [
        'autoFocus',
        'autoFocusEndDate',
        'initialStartDate',
        'initialEndDate',
        'showInputs',
        'isOutsideRange',
        'onOutsideClick'
    ]);
    return (
        calendarVisible && temporality === 'search' ? <div className="c-date-picker">
            <DayPickerRangeController
                {...propsF}
                onDatesChange={onDatesChange}
                onFocusChange={onFocusChange}
                onOutsideClick={cancelDates}
                isOutsideRange={date => isOutsideRange(
                    date,
                    props.initialStartDate
                )}
                renderCalendarInfo={() => (
                    <div className="c-date-picker__footer">
                        <button onClick={cancelDates}><span>Cancelar</span></button>
                        <button className={!states.DPDates.endDate ? 'disabled' : ''} onClick={setDates}><span>Aceptar</span></button>
                    </div>
                )}
                focusedInput={focusedInput}
                startDate={states.DPDates.startDate}
                endDate={states.DPDates.endDate}
            />
        </div>
            : null
    );
};

DatePicker.propTypes = propTypes;
DatePicker.defaultProps = defaultProps;

export default DatePicker;