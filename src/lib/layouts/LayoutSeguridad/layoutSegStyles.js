import { css } from 'glamor';
import calendarStyles from './calendarStyles';
import LightenDarkenColor from '../../Helpers/changeColors';

const darkenVariable = -20;
const lightenVariable = 20;

const stylesCommon = (titleColor, subtitleTheme, mainTheme) => {
    const subtitleColors = subtitleTheme.split(','); // Background, FontColor
    const mainColors = mainTheme.split(','); // Background, TitleFontColor, FontColor

    return css({
        position: 'relative',
        width: '100%',
        height: '100%',
        fontSize: 16,
        fontWeight: 300,
        ' *': {
            boxSizing: 'border-box'
        },
        // Container para mantener margen
        // ------------------------------
        ' .seg__container': {
            display: 'flex',
            width: '100%',
            margin: '0 auto',
            padding: '35px 40px 20px 40px',
            justifyContent: 'space-between',
            flexWrap: 'wrap',
        },
        // Contenedor principal
        // ------------------------------
        ' main': {
            width: '100vw',
            height: '100%',
            minWidth: '1024px',
            overflowY: 'auto',
            backgroundColor: LightenDarkenColor(mainColors[0], darkenVariable),
            ' header': {
                position: 'relative',
                zIndex: 10,
                width: '100%',
                height: 60,
                display: 'flex',
                justifyContent: 'flex-end',
                backgroundColor: '#f8f8f8',
                boxShadow: ' 0 2px 5px 0 rgba(0, 0, 0, 0.2)',
            },
            '@media (max-width: 1315px)': {
                ' .seg__container__topics': { width: '924px' },
                ' .topicSight__topic:not(:nth-child(3n))': {
                    marginRight: '25px'
                },
            },
            '@media (min-width: 1315px)': {
                ' .seg__container__topics': { width: '1235px' },
                ' .topicSight__topic:not(:nth-child(4n))': {
                    marginRight: '25px'
                }
            },
            '@media (min-width: 1632px)': {
                ' .seg__container__topics': { width: '1550px' },
                ' .topicSight__topic:not(:nth-child(4n))': {
                    marginRight: '0px'
                },
                ' .topicSight__topic:not(:nth-child(5n))': {
                    marginRight: '25px'
                }
            },
            '@media (min-width: 1948px)': {
                ' .seg__container__topics': { width: '1865px' },
                ' .topicSight__topic:not(:nth-child(5n))': {
                    marginRight: '0px'
                },
                ' .topicSight__topic:not(:nth-child(6n))': {
                    marginRight: '25px'
                }
            },
            '@media (min-width: 2264px)': {
                ' .seg__container__topics': { width: '2180px' },
                ' .topicSight__topic:not(:nth-child(6n))': {
                    marginRight: '0px'
                },
                ' .topicSight__topic:not(:nth-child(7n))': {
                    marginRight: '25px'
                },
            },
        },
        ' .seg__subtitle': {
            display: 'flex',
            height: 50,
            padding: '0px 45px',
            color: subtitleColors[1],
            backgroundColor: subtitleColors[0],
            alignItems: 'center',
            ' h1': {
                fontSize: '1.125em',
                fontWeight: 700,
                letterSpacing: '1.4px',
                textTransform: 'uppercase'
            },
            ' h2': {
                display: 'flex',
                fontSize: '0.8125em',
                fontWeight: 500,
                letterSpacing: '1px',
                alignItems: 'center',
                ' span': { marginLeft: 10 }
            },
            ' button': {
                color: subtitleColors[1]
            },
            ' svg g path:last-child': {
                fill: subtitleColors[1]
            },
        },
        ' .seg__subtitle__temporality': {
            position: 'relative',
            marginLeft: '30px',
            '&-info': {
                display: 'inline-block',
                marginLeft: 15,
                verticalAlign: 'middle',
                ' >h3': {
                    padding: '0px',
                    border: 'none',
                    fontSize: '0.769em',
                    textTransform: 'capitalize'
                },
                ' svg': {
                    margin: '0px 5px',
                    verticalAlign: 'middle'
                },
                ' svg path': {
                    fill: subtitleColors[1]
                },
                ' > p': {
                    width: 180,
                    margin: '5px 0px 0px',
                    fontSize: '0.92em',
                    fontWeight: 400,
                    color: subtitleColors[1]
                },
                ' > p span': {
                    margin: '0px'
                },
            },
            ' .c-date-picker': {
                position: 'absolute',
                top: '180%',
                right: '-25px',
                zIndex: 2,
                outline: 'none',
                ...calendarStyles(titleColor, mainColors, subtitleColors),
                '&__footer': {
                    display: 'flex',
                    padding: '0px 15px 7px',
                    justifyContent: 'space-between',
                },
                '&__footer .disabled': {
                    opacity: '0.5',
                },
                '&__footer button': {
                    padding: '5px 7px',
                    border: 'none',
                    borderRadius: '2.5px',
                    fontSize: '1.07em',
                    fontWeight: '400',
                    color: titleColor,
                    textTransform: 'uppercase',
                    backgroundColor: LightenDarkenColor(subtitleColors[0], darkenVariable),
                    cursor: 'pointer',
                    transition: 'background-color 0.25s ease-in-out'
                },
                '&__footer button:not(.disabled):hover': {
                    backgroundColor: subtitleColors[0],
                },
                '&__footer button:last-child': {
                    marginLeft: 'auto'
                },
            },
            ' >button': {
                display: 'inline-flex',
                width: '35px',
                height: '35px',
                backgroundColor: 'transparent',
                border: `1px ${LightenDarkenColor(mainColors[0], lightenVariable)} solid`,
                verticalAlign: 'middle',
                borderRadius: '2px',
                cursor: 'pointer',
                alignItems: 'center',
                justifyContent: 'center',
                '&:hover,&.active': {
                    backgroundColor: LightenDarkenColor(mainColors[0], lightenVariable),
                },
                '.button__realtime:not(.active):hover:after': {
                    backgroundColor: mainColors[0],
                    borderColor: mainColors[0],
                }
            },
            ' .button__realtime': {
                position: 'relative',
                paddingLeft: '14px',
                '&.active:after': {
                    background: '#7ed321',
                    borderColor: '#7ed321'
                },
                '&:after': {
                    content: '""',
                    position: 'absolute',
                    left: '3px',
                    top: '50%',
                    width: '4px',
                    height: '4px',
                    backgroundColor: LightenDarkenColor(mainColors[0], lightenVariable),
                    border: `1px ${LightenDarkenColor(mainColors[0], lightenVariable)} solid`,
                    borderRadius: '100%',
                    transform: 'translate(0px, -50%)'
                }
            }
        },
        // Módulo de no datos
        ' .noData': {
            height: '90%',
        },
        ' .no-data-module': {
            ' svg': { fill: mainColors[2] },
            ' h1': { color: mainColors[2] },
            paddingTop: '0 !important',
            justifyContent: 'center !important'
        },

        // Fixes especiales
        // ------------------------------
        ' .dot-map__chart.no-map .no-data-module': {
            backgroundColor: mainColors[0]
        },
        ' h3': {
            padding: '10px 25px',
            fontSize: '1em',
            fontWeight: 400,
            textAlign: 'left',
            textTransform: 'uppercase',
            color: mainColors[1],
            borderBottom: `solid 1px ${mainColors[3]}`
        },
        ' .tooltip_custom': {
            boxShadow: '2px 2px 10px rgba(0, 0, 0, .2)',
            fontSize: '0.75em',
            padding: 10,
            borderRadius: 2,
            letterSpacing: '1px',
            lineHeight: '20px',
            fontWeight: 500,
            textTransform: 'capitalize'
        },
        ' .topicSight__topic .m-timeline-trends': {
            width: '100%',
            ' .trend__header, .recharts-legend-wrapper': { display: 'none' },
        },
        '  .chart-line': {
            width: '100%',
            height: 360,
            margin: 0,
            padding: '20px 20px 40px 0px',
            '&-progress': {
                position: 'absolute',
                top: '50%',
                left: '50%',
                transform: 'translate(-50%, -50%)',
            },
        },
        ' .line-chart': {
            width: '100%',
            height: '360px',
            ' .noData': {
                height: '100% !important',
            },
            '&__chart': {
                width: '100%',
                ' .noData': {
                    height: '100% !important',
                },
                ' svg': {
                    ' text': {
                        fill: mainColors[2],
                        lineHeight: 1.4,
                        fontSize: '0.625em',
                    },
                    ' .recharts-cartesian-axis-line, .recharts-cartesian-axis-tick-line': {
                        stroke: 'transparent'
                    },
                    ' .recharts-cartesian-grid-horizontal line': {
                        strokeWidth: '0.6px',
                    }
                },
            },
            ' .column-data__label': {
                fontFamily: 'Roboto',
                fontSize: '0.68em !important',
                opacity: 0.5,
                stroke: mainColors[2],
                letterSpacing: '0.5px',
                textAnchor: 'middle',
            },
        },
        ' .feed-container-list': {
            margin: '15px auto !important',
            height: '840px !important',
            minHeight: '840px',
            ' .twitter-card': {
                backgroundColor: 'transparent',
                borderBottom: `solid 1px ${mainColors[3]}`,
                boxShadow: 'none',
                '&__user-image': {
                    borderRadius: '50%',
                    overflow: 'hidden'
                }
            },
            ' .container': {
                '&__header': {
                    justifyContent: 'start',
                },
                '&__header__name': {
                    color: mainColors[2],
                    fontSize: '0.75em'
                },
                '&__header__username': {
                    color: '#758795',
                    fontSize: '0.75em'
                },
                '&__header__datetime': {
                    color: '#758795',
                    marginLeft: 20
                },
                '&__sub-header': {
                    display: 'none'
                },
                '&__body': {
                    color: mainColors[2]
                },
                '&__body > a': {
                    color: mainColors[2]
                },
                '&__footer__sentiment h5': {
                    color: '#bbc9cf'
                },
                '&__footer__reactions svg path': {
                    stroke: mainColors[2]
                },
                '&__footer__reactions svg g': {
                    stroke: mainColors[2]
                },
            }
        },
        ' .feed-label': {
            maxWidth: 'none !important',
            backgroundColor: 'transparent !important',
            margin: '0px !important',
            padding: '0px !important',
            ' >h1': {
                padding: '10px 25px !important',
                fontSize: '1em !important',
                fontWeight: '400  !important',
                color: `${mainColors[1]}  !important`,
                textTransform: 'uppercase !important',
                borderBottom: `solid 1px ${mainColors[3]} !important`
            },
            ' svg': {
                display: 'none'
            }
        },
        ' .grid-gallery__images': {
            padding: '15px 20px',
            height: '961px',
            minHeight: '961px',
            overflowY: 'hidden',
            ' > .item': {
                position: 'relative',
                width: 'calc(33% - 2px) !important',
                marginBottom: '6px !important',
                opacity: 1,
                cursor: 'default',
                '::before': {
                    cursor: 'default',
                    opacity: 0
                }
            },
            ' .item__tag': {
                bottom: '5px !important',
                right: '5px !important',
                padding: '5px 7px',
                minWidth: '80px',
                color: '#FFF',
                backgroundColor: '#2491cc',
                ' span': {
                    marginLeft: '5px',
                    fontSize: '0.68em',
                    fontWeight: 600,
                }
            }
        }
    });
};

export default stylesCommon;
