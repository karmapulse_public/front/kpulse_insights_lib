import React, { useEffect, useState } from 'react';
import fetch from 'isomorphic-fetch';
import isUndefined from 'lodash/isUndefined';
import LoadingBoard from './LoadingBoard';
import ErrorWarroom from '../Components/ErrorWarroom';
import mapLayoutType from '../layouts/mapLayoutType';


const Warroom = props => {
    const [state, setState] = useState({
        loading: true,
        error: false,
        data: null,
        success: false,
        section: ''
    });

    const services = {
        develop: {
            config: 'https://dev-serverless.karmapulse.com/insights/get-board-configuration',
            twitter: 'https://dev-serverless.karmapulse.com/insights/recipes-twitter',
            facebook: 'https://dev-serverless.karmapulse.com/elasticsearch-fb/query_recipe_facebook'
        },
        production: {
            config: 'https://serverless.karmapulse.com/insights/get-board-configuration',
            twitter: 'https://serverless.karmapulse.com/insights/recipes-twitter',
            facebook: 'https://serverless.karmapulse.com/elasticsearch-fb/query_recipe_facebook'
        }
    };

    useEffect(() => {
        const {env, karmaBoard} = props;
        const fetchData = async () => {
        const uri = `${services[env].config}?content_type=board&fields.slug=${karmaBoard}&include=5`;
            const response = await fetch(uri);
            if (!response.ok) {
                setState({
                    ...state,
                    loading: false, error: true, data: response.statusText
                });
                return;
            }
            const json = await response.json();
            if (isUndefined(json.items[0])) {
                setState({
                    ...state,
                    loading: false, error: true, data: 'no existe información del board'
                });
                return;
            }

            const { section } = json.items[0].fields.layout_type[0].fields;

            setState({
                data: json.items[0], success: true, loading: false, section
            });

        };
    
        fetchData();
    }, []);
    if (state.loading === true) return <LoadingBoard />;
    if (state.error === true) return <ErrorWarroom status={state} />;
    return mapLayoutType(state.data.fields, `${props.sectionBoard || state.section}`, services[props.env])
};

export default Warroom;
