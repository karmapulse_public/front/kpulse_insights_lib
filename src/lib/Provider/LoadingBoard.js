import React from 'react';
import { css } from 'glamor';

const loader = css.keyframes({
    '0%': {
        transform: 'translate(-50%, -50%) scale(0.0)',
        opacity: 1
    },
    '100%': {
        transform: 'translate(-50%, -50%) scale(1.0)',
        opacity: 0
    }
});

const styles = css({
    width: '100vw',
    height: '100vh',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#000',
    ' .loader': {
        width: 70,
        height: 70,
        position: 'relative'
    },

    ' .dot1, .dot2': {
        width: 70,
        height: 70,
        position: 'absolute',
        top: '50%',
        left: '50%',
        borderRadius: '100%',
        border: '3px solid #fff',
        transform: 'translate(-50%, -50%) scale(0.0)',
        animation: `${loader} 1s infinite ease-in-out`
    },
    ' .dot2': {
        animationDelay: '.5s'
    }
});

const LoadingBoard = () => (
    <div className="loading-board" {...styles}>
        <div className="loader">
            <div className="dot1" />
            <div className="dot2" />
        </div>
    </div>
);

export default LoadingBoard;
