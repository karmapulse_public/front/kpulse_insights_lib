import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from './Helpers/ContextHook/context';
import Warroom from './Warroom';
import reset from './reset';
import 'typeface-roboto'

const MainRender = props => (
    <Provider>
        <div  {...reset} >
            <Warroom {...props}/>
        </div>
    </Provider>
)

MainRender.propTypes = {
    karmaBoard: PropTypes.string,
    env: PropTypes.oneOf(['develop', 'production'])
}
MainRender.defaultProps = {
    karmaBoard: null,
    env: 'production'
}

export default MainRender;
