import React from 'react';
import { Context } from './Helpers/ContextHook/context';
import Warroom from './Provider';

const MainRender = props => {
    const { state, dispatch } = React.useContext(Context);
    
    // React.useEffect(() => {
    //     const { env, karmaBoard } = props;
    //     const initialSection = () => {
    //         dispatch({ type: "CHANGESECTION", payload: karmaBoard })
    //     };

    //     initialSection();
    // }, []);
    return <Warroom {...Object.assign({}, { ...state }, { env: props.env, karmaBoard: props.karmaBoard})} />
};

export default MainRender;
