
export default () => ({
    container: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100vh',
        backgroundColor: 'rgb(53, 66, 73)',
        '& img': {
            width: 375,
            height: 342,
            objectFit: 'contain'
        },
        '& div': {
            marginLeft: 55,
            color: '#FFF',
            '& h1': {
                fontSize: 70,
                marginBottom: 10,
            },
            '& p': {
                width: 390,
                marginBottom: 40
            },
        },
        '& button': {
            fontWeight: 500,
            color: '#FFF',
            textTransform: 'uppercase',
            border: 'none',
            borderRadius: '2px',
            backgroundColor: '#3dad6e',
        }
    },
});
