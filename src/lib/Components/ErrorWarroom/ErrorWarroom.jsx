import React from 'react';
import PropTypes from 'prop-types';

const propTypes = {
    classes: PropTypes.object,
};

const defaultProps = {
    classes: {},
};

const ModuleError = () => (
    <section>
        <img src={`${process.env.PUBLIC_URL}/img/statusError@2x.png`} alt="Hubo un error" />
        <div>
            <h1>¡UPS!</h1>
            <p>El War Room indicado no existe. Verifica que la url sea la correcta o dale clic al siguiente botón:</p>
        </div>
    </section>
);

ModuleError.defaultProps = defaultProps;
ModuleError.propTypes = propTypes;

export default ModuleError;
