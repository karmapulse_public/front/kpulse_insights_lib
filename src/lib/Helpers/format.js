export const numberWithCommas = (num) => {
    const number = num || 0;
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
};
