export default function request(url, method = 'GET', params = {}, headers = {}) {
    return new Promise((resolve, reject) => {
        const req = new XMLHttpRequest();

        if (method === 'GET') {
            url = `${url}?index=${params.index}&es_query=${encodeURIComponent(JSON.stringify(params.es_query))}`;
        }

        req.open(method, url, true);
        req.setRequestHeader('Content-Type', 'application/json');

        if (headers) {
            Object.keys(headers).forEach((key) => {
                req.setRequestHeader(key, headers[key]);
            });
        }

        if (method === 'POST') {
            req.send(JSON.stringify(params));
        } else {
            req.send();
        }

        req.onreadystatechange = function handleResponse() {
            if ((this.responseText !== undefined && this.responseText !== ''
                && this.responseText !== null && this.responseText !== ' '
                && this.responseText[this.responseText.length - 1] === '}'
                && this.readyState === 4 && this.status === 200)
                || (this.readyState === 4 && this.status === 400)) {
                resolve({
                    json: JSON.parse(this.responseText),
                    status: {
                        info: this.statusText,
                        code: this.status
                    }
                });
            }
        };

        req.onerror = function handleError(error) {
            reject(error);
        };
    });
}

export function requestPost(url, method = 'GET', data) {
    return new Promise((resolve, reject) => {
        const rq = new XMLHttpRequest();

        rq.open(method, url, true);
        rq.setRequestHeader('Content-Type', 'application/JSON');
        rq.send(JSON.stringify(data));

        rq.onreadystatechange = function handleResponse() {
            if ((this.responseText !== undefined && this.responseText !== '')) {
                const response = {
                    json: JSON.parse(this.responseText),
                    status: {
                        info: this.statusText,
                        code: this.status
                    }
                };
                resolve(response);
            }
        };
        request.onerror = function handleError(error) {
            reject(error);
        };
    });
}

const convertData = (data) => {
    let string = '';
    let counter = 0;
    for (const prop in data) {
        const propName = prop.replace('_', '-');
        let value = data[prop];
        if (counter !== 0) {
            string += '&';
        }
        if (typeof value === 'object') {
            if (Array.isArray(value)) {
                value = value.join();
            } else {
                value = encodeURIComponent(JSON.stringify(value));
            }
        }
        string = string + propName + '=' + value;
        counter++;
    }
    return string;
};

export function fetchXHR(url, method, data) {
    return new Promise((resolve, reject) => {
        const request = new XMLHttpRequest();
        const urlParsed = method === 'GET' ? `${url}?${convertData(data)}` : url;

        request.open(method, urlParsed, true);
        request.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
        
        if (method === 'POST') {
            request.send(JSON.stringify(data));
        } else {
            request.send();
        }

        request.onreadystatechange = function handleResponse() {
            if ((this.responseText !== undefined && this.responseText !== ''
                && this.responseText !== null && this.responseText !== ' '
                && this.responseText[this.responseText.length - 1] === '}'
                && this.readyState === 4 && this.status === 200)
                || (this.readyState === 4 && this.status === 400)) {
                const response = {
                    json: JSON.parse(this.responseText),
                    status: {
                        info: this.statusText,
                        code: this.status
                    }
                };
                resolve(response);
            }
        };
        request.onerror = function handleError(error) {
            reject(error);
        };
    });
}
