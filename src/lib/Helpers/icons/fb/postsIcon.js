/**
 * PostsIcon SVG
 */

import React from 'react';

const PostsIcon = props => (
    /* eslint-disable */
    <svg
        width="18px"
        height="18px"
        viewBox="0 0 18 18"
        version="1.1"
        xmlnsXlink="http://www.w3.org/1999/xlink"
    >
        <g
            transform="translate(-20.000000, -309.000000) translate(20.000000, 309.000000)"
            fill={props.color}
            stroke="none"
            strokeWidth={1}
            fillRule="evenodd"
        >
            <path d="M2 2v14h13.997L16 2H2zm14 16H2c-1.103 0-2-.897-2-2V2C0 .897.897 0 2 0h14c1.103 0 2 .897 2 2v14c0 1.103-.897 2-2 2z" />
            <polygon points="9 3 10.714 7 15 7 11.292 10.416 13.285 15 9 12.165 4.714 15 6.708 10.416 3 7 7.285 7" />
        </g>
    </svg>

    /* eslint-enable */
);

export default PostsIcon;
