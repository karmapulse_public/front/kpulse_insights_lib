/**
 * ContentIcon SVG
 */

import React from 'react';

const ContentIcon = props => (
    /* eslint-disable */
    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="15" viewBox="0 0 20 15">
        <g fill={props.color} fillRule="evenodd">
            <path d="M0 15h20v-2H0zM12 6H8v6h4zM18 0h-4v12h4zM6 4H2v8h4z"/>
        </g>
    </svg>

    /* eslint-enable */
);

export default ContentIcon;
