import React from 'react';

const RealtimeIcon = ({ color }) => (
    /* eslint-disable */
    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="15" viewBox="0 0 16 15">
        <path fill={color} fillRule="evenodd" d="M13.818 8.067H10.91V5.378h2.91v2.69zm-4.363 0h-2.91V5.378h2.91v2.69zm0 4.034h-2.91V9.41h2.91v2.69zM5.09 8.067H2.18V5.378h2.91v2.69zm0 4.034H2.18V9.41h2.91v2.69zm9.454-10.756h-1.454V0h-1.455v3.361h-1.454V1.345H5.818V0H4.364v3.361H2.909V1.345H1.455C.652 1.345 0 1.948 0 2.689v10.084c0 .742.652 1.345 1.455 1.345h13.09c.803 0 1.455-.603 1.455-1.345V2.69c0-.74-.652-1.344-1.455-1.344z" />
    </svg>


    /* eslint-enable */
);

export default RealtimeIcon;
