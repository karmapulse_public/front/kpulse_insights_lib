/**
 * ProfileIcon SVG
 */

import React from 'react';

const ProfileIcon = props => (
    /* eslint-disable */
    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="19" viewBox="0 0 16 19">
        <g fill={props.color} fillRule="evenodd">
            <path d="M8 8C6.346 8 5 6.654 5 5h6c0 1.654-1.346 3-3 3m0-8C5.243 0 3 2.243 3 5s2.243 5 5 5 5-2.243 5-5-2.243-5-5-5M5.08 11.36C1.988 12.162 0 14.256 0 17v2h6.778L5.08 11.36zM10.92 11.36L9.222 19H16v-2c0-2.744-1.988-4.838-5.08-5.64M7 14l1-3 1 3-1 4z"/>
        </g>
    </svg>
    /* eslint-enable */
);

export default ProfileIcon;
