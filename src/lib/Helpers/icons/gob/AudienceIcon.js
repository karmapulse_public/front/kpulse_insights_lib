/**
 * AudienceIcon SVG
 */

import React from 'react';

const AudienceIcon = props => (
    /* eslint-disable */
    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="19" viewBox="0 0 20 19">
        <g fill={props.color} fillRule="evenodd">
            <path d="M19.76 16.807l-1.567-7.048c-.219-.986-1.183-1.76-2.193-1.76h-1.52C12.701 10.986 10 14 10 14s-2.702-3.014-4.48-6H4c-1.01 0-1.974.774-2.193 1.76L.24 16.807c-.123.554 0 1.112.337 1.532.337.42.856.66 1.423.66h16c.567 0 1.085-.24 1.422-.66.337-.42.46-.978.337-1.532"/>
            <path d="M8 4a2 2 0 1 1 4 0 2 2 0 0 1-4 0m6 0a4 4 0 0 0-8 0c0 2.21 4 7 4 7s4-4.79 4-7"/>
        </g>
    </svg>
    /* eslint-enable */
);

export default AudienceIcon;
