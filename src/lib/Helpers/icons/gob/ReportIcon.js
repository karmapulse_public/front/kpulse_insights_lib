/**
 * ScheduleIcon SVG
 */

import React from 'react';

const ScheduleIcon = props => (
    /* eslint-disable */
    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17">
        <path fill={props.color} fillRule="evenodd" d="M8 6H4V5h4v1zm0 3H2V8h6v1zm0 3H2v-1h6v1zM19 0h-2v9l-2-2-2 2V0h-1a.997.997 0 0 0-.707.293L10 1.586 8.707.293A.997.997 0 0 0 8 0H1a1 1 0 0 0-1 1v13a1 1 0 0 0 1 1h6.586l1.707 1.707a.997.997 0 0 0 1.414 0L12.414 15H19a1 1 0 0 0 1-1V1a1 1 0 0 0-1-1z"/>
    </svg>
    /* eslint-enable */
);

export default ScheduleIcon;
