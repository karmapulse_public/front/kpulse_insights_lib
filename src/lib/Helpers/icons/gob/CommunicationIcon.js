/**
 * CommunicationIcon SVG
 */

import React from 'react';

const CommunicationIcon = props => (
    /* eslint-disable */
    <svg xmlns="http://www.w3.org/2000/svg" width="21" height="18" viewBox="0 0 21 18">
        <path fill={props.color} fillRule="evenodd" d="M16 8h-6V6h6v2zm0 4h-6v-2h6v2zM7 8.25a1.25 1.25 0 1 1 0-2.5 1.25 1.25 0 0 1 0 2.5zm0 4a1.25 1.25 0 1 1 0-2.5 1.25 1.25 0 0 1 0 2.5zM11 0C5.486 0 1 4.038 1 9c0 1.735.541 3.392 1.573 4.836L0 18h11c5.514 0 10-4.038 10-9s-4.486-9-10-9z" />
    </svg>
    /* eslint-enable */
);

export default CommunicationIcon;
