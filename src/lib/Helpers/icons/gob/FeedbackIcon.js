/**
 * FeedbackIcon SVG
 */

import React from 'react';

const FeedbackIcon = props => (
    /* eslint-disable */
    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
        <g fill={props.color} fillRule="evenodd">
            <path d="M23.939 5.657A.999.999 0 0 0 23 5h-4.323L16.929.628a1.002 1.002 0 0 0-1.858 0L13.323 5H9a.999.999 0 0 0-.645 1.764l3.757 3.172-1.569 3.607 1.78 1.78L16 12.893l4.449 2.941a.996.996 0 0 0 1.194-.069.998.998 0 0 0 .274-1.164l-2.029-4.665 3.757-3.172a1 1 0 0 0 .294-1.107M19.924 18.617A1 1 0 0 0 19 18h-8.586l.293-.293a.999.999 0 0 0 0-1.414l-1-1a1.002 1.002 0 0 0-1.078-.222l-3.63 1.451V23h11c.266 0 .52-.106.708-.293l3-3a1 1 0 0 0 .217-1.09M3 15H1a1 1 0 0 0-1 1v7a1 1 0 0 0 1 1h2a1 1 0 0 0 1-1v-7a1 1 0 0 0-1-1"/>
        </g>
    </svg>
    /* eslint-enable */
);

export default FeedbackIcon;
