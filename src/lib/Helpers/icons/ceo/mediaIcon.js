import React from 'react';

const MediaIcon = props => (
    /* eslint-disable */
    <svg xmlns="http://www.w3.org/2000/svg" width="21" height="23" viewBox="0 0 21 23">
        <g fill={props.color} fillRule="evenodd">
            <path d="M18.79 9.986H21V5.548h-2.21zM5.526 15.534h2.21v1.257l-5.525.793v-3.159H0v7.768h2.21v-2.367L9 18.852c.543-.078.947-.546.947-1.099v-2.22h2.21v-2.217h-6.63v2.218zM15.474 7.766c0 1.226-.99 2.22-2.21 2.22H1.104v1.11c0 .612.495 1.109 1.106 1.109h14.368c.61 0 1.105-.497 1.105-1.11V5.298l-2.21.74v1.728z" />
            <path d="M19.895 0H4.42C1.983 0 0 1.99 0 4.437v3.33c0 .612.495 1.11 1.105 1.11h12.158c.61 0 1.105-.498 1.105-1.11v-2.53l5.876-1.966c.452-.15.756-.575.756-1.053v-1.11C21 .497 20.505 0 19.895 0" />
        </g>
    </svg>
    /* eslint-enable */
);

export default MediaIcon;