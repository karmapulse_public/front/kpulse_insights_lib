/**
 * DialogIcon SVG
 */

import React from 'react';

const AnalysisIcon = props => (
    /* eslint-disable */
    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
        <g fill={props.color} fillRule="evenodd">
            <path d="M17.001 17.931h-2v-5.17h2v5.17zm-4 0h-2V14.83h2v3.102zm-4.001 0H7v-5.17h2v5.17zm-4 0H3V14.83h2v3.102zm11-8.879L12.083 5l-4 5.171L5 6.984 3.414 8.623H0v9.308C0 19.072.897 20 2 20h16c1.103 0 2-.928 2-2.069V6.555h-1.586L16 9.052z"/>
            <path d="M18 0H2C.897 0 0 .956 0 2.132v5.332h2.586L5 4.89 7.917 8l4-5.331L16 7.022l1.586-1.69H20v-3.2C20 .956 19.103 0 18 0"/>
        </g>
    </svg>

    /* eslint-enable */
);

export default AnalysisIcon;
