/**
 * DialogIcon SVG
 */

import React from 'react';

const AnalysisIcon = props => (
    /* eslint-disable */
    <svg xmlns="http://www.w3.org/2000/svg" width="17" height="20" viewBox="0 0 17 20">
        <g fill={props.color} fillRule="evenodd">
            <path d="M0 20h17v-2H0zM10 11H7v7h3zM12 18h3V8h-3zM5 14H2v3h3zM1.25 12l9.982-9.214v2.11H13V0H7.696v1.632h2.286L0 10.846z" />
        </g>
    </svg>

    /* eslint-enable */
);

export default AnalysisIcon;
