import moment from 'moment';
import 'moment/locale/es';
moment.locale('es');
const initialState = {
    dateRange: {
        startDate: moment(),
        endDate: moment()
    },
    sectionBoard : '',
    initiativeSelected: {},
    disabledCalendar: false, // va sobre el componente de Temporalidad
    temporality: 'realtime',
    calendarVisible: false // Va sobre el componente de DP
};

export default initialState;
