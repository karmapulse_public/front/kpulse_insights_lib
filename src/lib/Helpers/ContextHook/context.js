import React from 'react';
import initialState from './initialState';
import reducer from './reducer';

const Context = React.createContext();
const Consumer = Context.Consumer;

const Provider = (props) => {
    const [state, dispatch] = React.useReducer(reducer, initialState);
    const value = { state, dispatch };

    /* eslint-disable */
    return (
        <Context.Provider value={value}>
            {props.children}
        </Context.Provider>
    )
    /* eslint-enable */

}

export { Context, Consumer, Provider };
