import initialState from './initialState';

const RESET = 'RESET';
const CHANGEDATE = 'CHANGEDATE';
const SELECTINITIATIVE = 'SELECTINITIATIVE';
const CHANGETEMPORALITY = 'CHANGETEMPORALITY';
const SETDISABLEDCALENDAR = 'SETDISABLEDCALENDAR';
const CHANGESECTION = 'CHANGESECTION';
const CHANGECALENDARVISIBILITY = 'CHANGECALENDARVISIBILITY';

const reducer = (state, action) => {
    switch (action.type) {
    case RESET:
        return initialState;
    case CHANGEDATE:
        return {
            ...state, dateRange: action.payload
        };
    case CHANGESECTION:
        return {
            ...state, sectionBoard: action.payload
        };
    case SELECTINITIATIVE:
        return {
            ...state, initiativeSelected: action.payload
        };
    case SETDISABLEDCALENDAR:
        return {
            ...state, disabledCalendar: action.payload
        };
    case CHANGETEMPORALITY:
        return {
            ...state, temporality: action.payload
        };
    case CHANGECALENDARVISIBILITY:
        return {
            ...state, calendarVisible: action.payload
        };
    default:
        return initialState;
    }
};

export default reducer;
