import React, { Component } from 'react';
import moment from 'moment';
import { withConnect } from '../../../helpers/context';

export default ({ disabled }) => (Componente) => {
    class HOCSelectTime extends Component {
        constructor(props) {
            super(props);
            this.interval = null;
            this.modifyDateRange = this.modifyDateRange.bind(this);
        }

        componentDidMount() {
            clearInterval(this.interval);
            if (disabled !== undefined) {
                this.props.changeDisabled(disabled);
            } else {
                if (this.props.temporality === 'realtime') {
                    this.interval = setInterval(() => this.modifyDateRange(), 60000);
                }
            }
        }

        componentWillReceiveProps(nextProps) {
            const { temporality, dateRange } = nextProps;
            if (nextProps.changeDisabled !== this.props.changeDisabled) {
                nextProps.changeDisabled(disabled);
            }
            if (temporality && dateRange && nextProps.temporality !== this.props.temporality) {
                if (temporality === 'search') {
                    clearInterval(this.interval);
                    if (nextProps.dateRange !== this.props.dateRange) {
                        this.props.changeDateRange({
                            startDate: nextProps.dateRange.startDate,
                            endDate: nextProps.dateRange.endDate
                        });
                    }
                } else {
                    this.props.changeDateRange({
                        startDate: moment().subtract(8, 'd').set({
                            minute: 0,
                            second: 0,
                            millisecond: 0
                        }),
                        endDate: moment()
                    });
                    this.interval = setInterval(() => this.modifyDateRange(), 60000);
                }
            }
        }

        modifyDateRange() {
            let { startDate, endDate } = this.props.dateRange;
            this.props.changeDateRange({
                startDate: startDate.add(1, 'm').set({
                    minute: 0,
                    second: 0,
                    millisecond: 0
                }),
                endDate: endDate.add(1, 'm')
            });
        }

        componentWillUnmount() {
            clearInterval(this.interval);
        }

        render() {
            return (
                <Componente
                    dateRange={this.props.dateRange}
                    {...this.props}
                />
            );
        }
    }

    const mapStateToProps = (state) => ({
        dateRange: state.dateRange === undefined ? {
            startDate: moment().subtract(8, 'd').set({
                minute: 0,
                second: 0,
                millisecond: 0
            }),
            endDate: moment()
        } : state.dateRange,
        temporality: state.temporality === undefined ? 'realtime' : state.temporality,
        disabledCalendar: state.disabled === undefined ? false : state.disabled,
    });

    const mapDispatchToProps = (state) => ({
        changeDateRange: state.changeDateRange || (() => ({})),
        changeDisabled: state.changeDisabled || (() => ({})),
        goToUrl: state.goUrl,

    });

    return withConnect(mapStateToProps, mapDispatchToProps)(HOCSelectTime);
};
